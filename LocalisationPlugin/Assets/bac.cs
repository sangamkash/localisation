﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class bac : MonoBehaviour
{
    // Start is called before the first frame update
    [ContextMenu("CheckPrefab")]
    void Start()
    {
        bool isPrefabInstance = PrefabUtility.GetCorrespondingObjectFromSource(gameObject) != null && PrefabUtility.GetPrefabInstanceHandle(gameObject.transform) != null;
        bool isPrefabOriginal = PrefabUtility.GetCorrespondingObjectFromSource(gameObject) == null && PrefabUtility.GetPrefabInstanceHandle(gameObject.transform) != null;
        bool isDisconnectedPrefabInstance = PrefabUtility.GetCorrespondingObjectFromSource(gameObject) != null && PrefabUtility.GetPrefabInstanceHandle(gameObject.transform) == null;
        var isChanged=PrefabUtility.GetObjectOverrides(gameObject);
        var addedComponents=PrefabUtility.GetAddedComponents(gameObject);
        var addedGameObjs = PrefabUtility.GetAddedGameObjects(gameObject);
        for (int i = 0; i < isChanged.Count; i++)
        {
            Debug.Log(isChanged[i].GetAssetObject().name);
        }
        for (int i = 0; i < addedComponents.Count; i++)
        {
            Debug.Log($"type ::{addedComponents[i].GetType()}");
        }
        for (int i = 0; i < addedGameObjs.Count; i++)
        {
            Debug.Log(addedGameObjs[i].GetAssetObject().name);
        }
        Debug.Log($"isPrefabInstance ::{isPrefabInstance} && isPrefabOriginal::{isPrefabOriginal} && isDisconnectedPrefabInstance::{isDisconnectedPrefabInstance} && isChanged{isChanged.Count}");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
