﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
//using Boo.Lang;
using UnityEditor;
using UnityEngine;
using Newtonsoft.Json;
using Object = UnityEngine.Object;
using MagnasStudio.Localisation.Constant;
using UnityEditor.SceneManagement;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.SceneManagement;

namespace MagnasStudio.Editors.Localization
{
    public class LocalisationAssignSceneEditor : EditorWindow
    {
        public static List<SceneAsset> sceneAssets
        {
            get { return MagnasLocalisationData.Instance.sceneAssets; }
            set { MagnasLocalisationData.Instance.sceneAssets = value; }
        }

        public static List<string> scenePath
        {
            get { return MagnasLocalisationData.Instance.scenePath; }
            set { MagnasLocalisationData.Instance.scenePath = value; }
        }

        private static LocalisationAssignSceneEditor window;
        public static bool subs = false;
        public static bool openingScene = false;
        Vector2 scrollPos;

        public static void ShowWindow()
        {
            window = (LocalisationAssignSceneEditor) EditorWindow.GetWindow(typeof(LocalisationAssignSceneEditor), true,
                "Localisation");
            window.ShowPopup();
            subs = false;
            openingScene = false;
        }


        void OnGUI()
        {
            var w = window.position.width;
            var h = window.position.height;
            var cellHeight = 30;
            var buttonHeight = 40;
            GUILayout.Label("Scene used in Game", EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("+", GUILayout.Height(buttonHeight)))
            {
                if (sceneAssets == null)
                    sceneAssets = new List<SceneAsset>();
                sceneAssets.Add(null);
            }
           
            if (GUILayout.Button("Load from build setting", GUILayout.Height(buttonHeight)))
            {
                int length = EditorSceneManager.sceneCountInBuildSettings;
                sceneAssets = new List<SceneAsset>();
                for (int i = 0; i < length; i++)
                {
                    var scenePath = SceneUtility.GetScenePathByBuildIndex(i);
                    var sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath);
                    sceneAssets.Add(sceneAsset);
                }
            }
            if (GUILayout.Button("Remove Duplicate and null", GUILayout.Height(buttonHeight)))
            {
                RemoveDuplicateAndNull();
            }

            EditorGUILayout.EndHorizontal();
            var count = sceneAssets == null ? 0 : sceneAssets.Count;
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(w), GUILayout.Height(h));
            //===
            if (sceneAssets != null)
            {
                EditorGUILayout.BeginVertical();
                for (int i = 0; i < count; i++)
                {
                    EditorGUILayout.BeginHorizontal();
                    sceneAssets[i] = (SceneAsset) EditorGUILayout.ObjectField(sceneAssets[i], typeof(SceneAsset), true,
                        RerturnGUILayoutOption(w * .8f, cellHeight));
                    if (GUILayout.Button("-", RerturnGUILayoutOption(w * .2f, cellHeight)))
                    {
                        sceneAssets.Remove(sceneAssets[i]);
                        count--;
                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.EndVertical();
            }

            EditorGUILayout.EndScrollView();


        }
        private GUILayoutOption[] RerturnGUILayoutOption(float w, float h)
        {
            var layoutOptions = new System.Collections.Generic.List<GUILayoutOption>();
            layoutOptions.Add(GUILayout.Width(w));
            layoutOptions.Add(GUILayout.Height(h));

            return layoutOptions.ToArray();
        }

        private void RemoveDuplicateAndNull()
        {
            if(sceneAssets== null)
                return;
            var count = sceneAssets.Count;
            // var remove=new List<SceneAsset>();
            for (var i = 0; i < count; i++)
            {
                if (sceneAssets[i] == null)
                {
                    sceneAssets.Remove(sceneAssets[i]);
                    count--;
                    i--;
                    continue;
                }
                for (int j = i+1; j < count; j++)
                {
                    if (sceneAssets[i] == sceneAssets[j])
                    {
                        sceneAssets.Remove(sceneAssets[j]);
                        count--;
                        i--;
                        break;
                    }
                }
            }
            
        }    

        private void UpdateScenePath()
        {
            if(sceneAssets== null)
                return;
            List<string> paths=new List<string>();
            var count=sceneAssets.Count;
            for (int i = 0; i < count; i++)
            {
                var path=AssetDatabase.GetAssetPath(sceneAssets[i]);
                paths.Add(path);
            }

            scenePath = paths;
        }
        
        private void OnDestroy()
        {
            RemoveDuplicateAndNull();
            UpdateScenePath();
        }
    }
}