﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading;
using System.Threading.Tasks;
using MagnasStudio.Localisation;
using MagnasStudio.Localisation.Constant;
using MagnasStudio.Util.Editors;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MagnasStudio.Editors.Localization
{
    public class LocalisationViewEditor : EditorWindow
    {
        public SupportedLanguageScriptableObject supportedLanguageScriptableObject;
        private Dictionary<int,CompleteMetaData> _allLanguagesMeta
        {
            get { return MagnasLocalisationData.Instance._allLanguagesMeta; }
        }

        private bool showMetaData
        {
            get { return MagnasLocalisationData.Instance.showMetaData; }
            set { MagnasLocalisationData.Instance.showMetaData = value; }
        }

        private int totalcount
        {
            get { return MagnasLocalisationData.Instance.totalcount; }
        }
        private SearchField mSearchField;
        private List<LocalMetaData> headingList;
        private Vector2 scrollPos,scrollPosH,scrollPosK;
        private Vector2 scrollPosOld,scrollPosHOld,scrollPosKOld;
        private static LocalisationViewEditor window;
        private int displayCount=5;
        private int page = 0;
        private float ScaleFactor = 1;
        private float keyCellWidth => 100*ScaleFactor;
        private float metaCellWidth => 400*ScaleFactor;
        private float languageCellWidth => 300*ScaleFactor;
        private float cellHeight => 60*ScaleFactor;
        //const string
        private const string ShowMetaDataStr = "Show Meta data";
        private const string ScaleFactorStr = "Scale Factor";
        private const string ModifyStr = "Modify";
        private const string KeyStr = "Key";
        private const string MetaDataStr = "MetaData";
        private const string ModifyFunStr = "Modify";
        private const string SearchStr = "search";
        private const string ClearStr = "Clear";
        private const string CaseSensitiveStr = "case sensetive";
        private const string DisplayCount = "DisplayCount ";
        private const string PageCount = "PageCount ";
        private const string GotoStr = "Goto";

        private bool caseSensitive = false;
        private bool searching = false;
        private string searchItem = "";
        private int selected;
        private List<SupportedLanguage> options;
        private string[] optionsStr;
        private GUIStyle styleHeading = new GUIStyle();
        private GUIStyle styleLeftTextAlignment  = new GUIStyle();
        private Dictionary<int, CompleteMetaData> allLanguage;
        private List<int> searchedLanguage;
        private static int searchCount = 0;

        private Vector2[] listOfMetaScroll;
        //Thread
        private Thread threadSearching;
        private string threadSearchItem;
        private SupportedLanguage threadSelectedLanguage;
        private SupportedLanguage selectedLanguage;
        private float searchProgress;
        private bool threadCaseSensitive;
        private ObjectInfo highLightObject;
        public static void ShowWindow(SupportedLanguageScriptableObject supportedLanguageScriptableObject)
        {

            window = GetWindow<LocalisationViewEditor>("Localisation Viewer");
            window.supportedLanguageScriptableObject = supportedLanguageScriptableObject;
            window.CreateHeading();
            window.styleHeading= new GUIStyle();
            window.styleHeading.alignment = TextAnchor.MiddleCenter;
            window.styleHeading.fontStyle = FontStyle.Bold;
            window.styleHeading.normal.textColor = Color.blue;
            window.styleLeftTextAlignment= new GUIStyle();
            window.styleHeading.alignment = TextAnchor.UpperLeft;
            var count = supportedLanguageScriptableObject.supportedLanguages.Count;
           
            window.options= new List<SupportedLanguage>();
            window.options.Add(SupportedLanguage.None);
            window.options.Add(supportedLanguageScriptableObject.defaultLanguageType);
            for (var i = 0; i < count; i++)
            {
                window.options.Add(supportedLanguageScriptableObject.supportedLanguages[i]);
            }

            count = window.options.Count;
            window.optionsStr= new string[count];
            for (var i = 0; i < count; i++)
            {
                window.optionsStr[i] = window.options[i].ToString();
            }
            window.allLanguage = window._allLanguagesMeta;
            window.searchedLanguage = new List<int>();
        }

        private void OnEnable()
        {
            mSearchField = new SearchField();
        }

        void OnGUI()
        {
            var w = window.position.width;
            var h = window.position.height;
            DrawHeader();
            //===============================================================
            DrawSearchProgress();

            EditorGUILayout.Space(15);
            scrollPosH.y = 0;
            scrollPosH=EditorGUILayout.BeginScrollView(scrollPosH,GUILayout.Height(50));// 1---------------------------------------------
           
            
            Heading(selectedLanguage);
            EditorGUILayout.EndScrollView();//1------------------------------------------------------------------
            if (scrollPosH.x != scrollPosHOld.x)
                scrollPos.x = scrollPosH.x;
            scrollPosK.x = 0;
            EditorGUILayout.BeginHorizontal(); //===============================================================
            scrollPosK=EditorGUILayout.BeginScrollView(scrollPosK,GUILayout.Width(keyCellWidth*2));//2------
            if(!searching)
            {
                ShowKeyValues(allLanguage,totalcount,displayCount,page,selectedLanguage);
            }
            else
            {
                ShowKeyValuesFilter(allLanguage,searchedLanguage,displayCount,page,selectedLanguage);
            }
            EditorGUILayout.EndScrollView();//2--------------------------------------------------------------------------
            if (scrollPosK.y != scrollPosKOld.y)
                scrollPos.y = scrollPosK.y;
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);//3---------------------------------------------------------
            if (_allLanguagesMeta != null)
            {
                if(!searching)
                {
                    ShowLocalValues(allLanguage,totalcount,displayCount,page,selectedLanguage);
                }
                else
                {
                    ShowLocalValuesFilter(allLanguage, searchedLanguage, displayCount, page, selectedLanguage);
                }
            }
            EditorGUILayout.EndScrollView();//3---------------------------------------------------------------------------------
            EditorGUILayout.EndHorizontal();//===============================================================
            if (scrollPos.y != scrollPosOld.y)
                scrollPosK.y = scrollPos.y;
            if (scrollPos.x != scrollPosOld.x)
                scrollPosH.x = scrollPos.x;
            scrollPosOld=scrollPos;
            scrollPosHOld=scrollPosH;
            scrollPosKOld=scrollPosK;
            if (highLightObject != null)
            {
                var obj = highLightObject;
                highLightObject = null;
                HighLightObject(obj);
            }
        }

        private void DrawSearchProgress()
        {
            if (searching)
            {
                EditorGUI.ProgressBar(new Rect(3, 45, position.width - 6, 20), searchProgress,
                    ((int) (searchProgress * 100f)).ToString());
                EditorGUILayout.Space(25);
            }
        }

        private void DrawHeader()
        { 
            EditorGUILayout.BeginHorizontal(); //===============================================================
            GUILayout.Label(ShowMetaDataStr, EditorStyles.boldLabel,GUILayout.Width(100));
            showMetaData = EditorGUILayout.Toggle(showMetaData,GUILayout.Width(25));
            GUILayout.Label(ScaleFactorStr, EditorStyles.boldLabel,GUILayout.Width(100));
            ScaleFactor=GUILayout.HorizontalSlider(ScaleFactor, 1f, 4f, GUILayout.Width(100));
            //tempcout = EditorGUILayout.FloatField("Count", tempcout,GUILayout.Width(40));
            GUILayout.Label(DisplayCount, EditorStyles.boldLabel,GUILayout.Width(100));
            var displayValue = (int)GUILayout.HorizontalSlider(displayCount, 2,400,GUILayout.Width(100));
            if (displayCount != displayValue)
            {
                displayCount = displayValue;
                page = 0;
            }
            searchItem=mSearchField.OnToolbarGUI(searchItem, GUILayout.Width(250),GUILayout.Height(30));
            GUILayout.Label(CaseSensitiveStr, EditorStyles.boldLabel,GUILayout.Width(100));
            caseSensitive = EditorGUILayout.Toggle(caseSensitive,GUILayout.Width(25));
            EditorGUI.BeginChangeCheck();
            selected = EditorGUILayout.Popup("Language", selected, optionsStr,GUILayout.Width(150));
            selectedLanguage = GetSelectedLanguage(selected);
            if (EditorGUI.EndChangeCheck())
            {
                Debug.Log(GetSelectedLanguage(selected).ToString());
                Debug.Log(searchItem);
            }
            var buttonText = searching == true ? ClearStr : SearchStr;
            if (GUILayout.Button(buttonText,GUILayout.Width(50)))
            {
                searching = !searching;
                if (searching)
                {
                    if (threadSearching != null)
                        threadSearching.Abort();
                    threadSearchItem = searchItem;
                    threadSelectedLanguage = selectedLanguage;
                    threadCaseSensitive = caseSensitive;
                    searchProgress = 0f;
                    threadSearching = new Thread(ThreadSearch);
                    threadSearching.Start();
                     page = 0;
                }
                else
                {
                    if (threadSearching != null && threadSearching.IsAlive)
                    {
                        threadSearching.Abort();
                    }
                }
            }
            var _totalcount = searching ? searchedLanguage.Count : totalcount;
            var totalpage = Mathf.CeilToInt((float) _totalcount / (float) displayCount);
            if (GUILayout.Button("<",GUILayout.Width(20)))
            {
                page--;
                page = Mathf.Clamp(page, 0, totalpage);
            }

            GUILayout.Label(page+1 + "/" + totalpage, EditorStyles.boldLabel, GUILayout.Width(50));
            if (GUILayout.Button(">",GUILayout.Width(20)))
            {
                page++;
                page = Mathf.Clamp(page, 0, totalpage-1);
            }
            EditorGUILayout.EndHorizontal();
        }
        private void ShowLocalValues(Dictionary<int,CompleteMetaData> _allLanguagesMeta,int totalCount,int displayCount,int CurrentPage,SupportedLanguage supportedLanguage=SupportedLanguage.None)
        {
            EditorGUILayout.BeginVertical(); //BV 1
            var startingIndex = displayCount * CurrentPage;
            var maxCount = Mathf.Clamp(displayCount * (CurrentPage + 1), 0, totalCount);
            if (listOfMetaScroll == null)
            {
                listOfMetaScroll = new Vector2[maxCount];
            }
            else if(listOfMetaScroll.Length != maxCount)
            {
                listOfMetaScroll = new Vector2[maxCount];
            }
            for (int j = startingIndex; j < maxCount; j++)
            {
                KeyValuePair<int, CompleteMetaData> item = new KeyValuePair<int, CompleteMetaData>(j + 1, _allLanguagesMeta[j + 1]);
           
                EditorGUILayout.BeginHorizontal(); //BH 2
                var cc = -1;
                var locationData = item.Value.locations;
                foreach (var subItem in item.Value.localMetaDatas)
                {
                    cc++;
                    if (cc == 0 && showMetaData)
                    {
                        listOfMetaScroll[j]=EditorGUILayout.BeginScrollView(listOfMetaScroll[j],GUILayout.Width(metaCellWidth), GUILayout.Height(cellHeight));
                        var count = locationData.Count;
                        for (var i = 0; i < count; i++)
                        {
                            EditorGUILayout.BeginHorizontal();
                            if (GUILayout.Button(GotoStr,GUILayout.Width(metaCellWidth*0.2f)))
                            {
                                highLightObject = locationData[i];
                            }
                            EditorGUILayout.LabelField($"Type:: {locationData[i].type}",styleHeading,GUILayout.Width(metaCellWidth*0.2f));
                            EditorGUILayout.TextArea(locationData[i].location,GUILayout.Width(metaCellWidth*0.6f));
                            EditorGUILayout.EndHorizontal();
                        }
                        EditorGUILayout.EndScrollView();
                    }
                    EditorGUILayout.TextArea(subItem.value, GUILayout.Width(languageCellWidth), GUILayout.Height(cellHeight));
                }

                EditorGUILayout.EndHorizontal(); //BH 2
            }

            EditorGUILayout.EndVertical(); //BV 1
        }

        private void ShowKeyValues(Dictionary<int,CompleteMetaData> _allLanguagesMeta,int totalCout,int displayCount,int CurrentPage,SupportedLanguage selectedLanguage=SupportedLanguage.None,string searchItem="",bool caseSensitive =false)
        {
            var startingIndex = displayCount * CurrentPage;
            var maxCount = Mathf.Clamp(displayCount * (CurrentPage + 1), 0, totalCout);
                
            for (int j = startingIndex; j < maxCount; j++)
            {
                KeyValuePair<int, CompleteMetaData> item =
                    new KeyValuePair<int,CompleteMetaData>(j + 1, _allLanguagesMeta[j + 1]);
            
                EditorGUILayout.BeginVertical();
                EditorGUILayout.BeginHorizontal();
                var show = false;
                {
                    foreach (var subItem in item.Value.localMetaDatas)
                    {
                        var list = item.Value.localMetaDatas;
                        var count = list.Count;
                        var searchItemEmpty = searchItem == "";
                        
                        if (selectedLanguage == SupportedLanguage.None)
                        {
                                show = true;
                                break;
                        }
                        else if (subItem.supportedLanguage == SupportedLanguage.None ||
                                 subItem.supportedLanguage == selectedLanguage)
                        {
                                show = true;
                                break;
                        } 
                    }
                    if (show)
                    {
                        EditorGUILayout.IntField(item.Key, GUILayout.Width(keyCellWidth - 10f),
                            GUILayout.Height(cellHeight));
                        if (GUILayout.Button(ModifyStr, GUILayout.Width(keyCellWidth),
                            GUILayout.Height(cellHeight)))
                        {
                            ModifyKey(item.Key);
                        }
                    }
                }

                EditorGUILayout.EndHorizontal();
                EditorGUILayout.EndVertical();
            }
        }
       
        private void ShowLocalValuesFilter(Dictionary<int,CompleteMetaData> _allLanguagesMeta,List<int> filterList,int displayCount,int CurrentPage,SupportedLanguage supportedLanguage=SupportedLanguage.None)
        {
            EditorGUILayout.BeginVertical(); //BV 1
            var totalCout = filterList.Count;
            var startingIndex = displayCount * CurrentPage;
            var maxCount = Mathf.Clamp(displayCount * (CurrentPage + 1), 0, totalCout);
            if (listOfMetaScroll == null)
            {
                listOfMetaScroll = new Vector2[maxCount];
            }
            else if(listOfMetaScroll.Length != maxCount)
            {
                listOfMetaScroll = new Vector2[maxCount];
            }
            for (int j = startingIndex; j < maxCount; j++)
            {
                var temp = _allLanguagesMeta[filterList[j]];
                KeyValuePair<int,CompleteMetaData> item =
                    new KeyValuePair<int, CompleteMetaData>(filterList[j], temp);
                EditorGUILayout.BeginHorizontal(); //BH 2
                var locationData = item.Value.locations;
                var cc = -1;
                foreach (var subItem in item.Value.localMetaDatas)
                {
                    cc++;
                    if (cc == 1)
                        continue;
                    if (cc == 0 && showMetaData)
                    {
                        listOfMetaScroll[j] = EditorGUILayout.BeginScrollView(listOfMetaScroll[j],GUILayout.Width(metaCellWidth), GUILayout.Height(cellHeight));
                        var count = locationData.Count;
                        for (var i = 0; i < count; i++)
                        {
                            EditorGUILayout.BeginHorizontal();
                            if (GUILayout.Button(GotoStr,GUILayout.Width(metaCellWidth*0.2f)))
                            {
                                highLightObject = locationData[i];
                            }
                            EditorGUILayout.LabelField($"Type:: {locationData[i].type}",styleHeading,GUILayout.Width(metaCellWidth*0.2f));
                            EditorGUILayout.TextArea(locationData[i].location,GUILayout.Width(metaCellWidth*0.6f));
                            EditorGUILayout.EndHorizontal();
                        }
                        EditorGUILayout.EndScrollView();
                    }
                    if (supportedLanguage == SupportedLanguage.None)
                    {
                         EditorGUILayout.TextArea(subItem.value, GUILayout.Width(languageCellWidth), GUILayout.Height(cellHeight));
                       
                    }
                    else if (subItem.supportedLanguage == SupportedLanguage.None || subItem.supportedLanguage == supportedLanguage)
                    {
                            EditorGUILayout.TextArea(subItem.value, GUILayout.Width(languageCellWidth), GUILayout.Height(cellHeight));
                    }
                    
                }

                EditorGUILayout.EndHorizontal(); //BH 2
            }

            EditorGUILayout.EndVertical(); //BV 1
        }

        private void ShowKeyValuesFilter(Dictionary<int,CompleteMetaData> _allLanguagesMeta,List<int> filterList,int displayCount,int CurrentPage,SupportedLanguage selectedLanguage=SupportedLanguage.None,string searchItem="",bool caseSensitive =false)
        {
            var totalCout=filterList.Count;
            var startingIndex = displayCount * CurrentPage;
            var maxCount = Mathf.Clamp(displayCount * (CurrentPage + 1), 0, totalCout);
            for (int j = startingIndex; j < maxCount; j++)
            {
                var temp = _allLanguagesMeta[filterList[j]];
                KeyValuePair<int, CompleteMetaData> item =
                    new KeyValuePair<int, CompleteMetaData>(filterList[j], temp);
                EditorGUILayout.BeginVertical();
                EditorGUILayout.BeginHorizontal();
                var show = false;
                {
                    foreach (var subItem in item.Value.localMetaDatas)
                    {
                        var list = item.Value.localMetaDatas;
                        var count = list.Count;
                        
                        if (selectedLanguage == SupportedLanguage.None)
                        {
                                show = true;
                                break;
                           
                        }
                        else if (subItem.supportedLanguage == SupportedLanguage.None ||
                                 subItem.supportedLanguage == selectedLanguage)
                        {
                                show = true;
                                break;
                        } 
                    }
                    if (show)
                    {
                        EditorGUILayout.IntField(item.Key, GUILayout.Width(keyCellWidth - 10f),
                            GUILayout.Height(cellHeight));
                        if (GUILayout.Button(ModifyStr, GUILayout.Width(keyCellWidth),
                            GUILayout.Height(cellHeight)))
                        {
                            ModifyKey(item.Key);
                        }
                    }
                }

                EditorGUILayout.EndHorizontal();
                EditorGUILayout.EndVertical();
            }
        }
       
        private void Heading(SupportedLanguage supportedLanguage=SupportedLanguage.None ,string searchItem="")
        {
            EditorGUILayout.BeginHorizontal(); //BH 1
            var hCount = headingList.Count;
            for (var i = 0; i < hCount; i++)
            {
                var cw = languageCellWidth;
                switch (i)
                {
                    case 0:
                    case 1:
                        cw = keyCellWidth;
                        break;
                    case 2 when showMetaData :
                        if (searchItem == "")
                            cw = metaCellWidth;
                        else
                            continue;
                        break;
                    case 2 when !showMetaData:
                        continue;
                }
                if (supportedLanguage == SupportedLanguage.None)
                {
                    EditorGUILayout.LabelField(headingList[i].value, styleHeading, GUILayout.Width(cw));
                }
                else if (headingList[i].supportedLanguage == SupportedLanguage.None || headingList[i].supportedLanguage == supportedLanguage)
                {
                    EditorGUILayout.LabelField(headingList[i].value, styleHeading, GUILayout.Width(cw));
                }

            }
            EditorGUILayout.EndHorizontal(); //BH 1
        }

        private void CreateHeading()
        {
            headingList= new List<LocalMetaData>();
            var count = supportedLanguageScriptableObject.supportedLanguages.Count;
            headingList.Add(new LocalMetaData(SupportedLanguage.None, KeyStr));
            headingList.Add(new LocalMetaData(SupportedLanguage.None,ModifyFunStr));
            headingList.Add(new LocalMetaData(SupportedLanguage.None,MetaDataStr));
            headingList.Add(new LocalMetaData(supportedLanguageScriptableObject.defaultLanguageType,supportedLanguageScriptableObject.defaultLanguageType.ToString()));
            for (int i = 0; i < count; i++)
            {
                if(supportedLanguageScriptableObject.defaultLanguageType == supportedLanguageScriptableObject.supportedLanguages[i])
                    continue;
                headingList.Add(new LocalMetaData(supportedLanguageScriptableObject.supportedLanguages[i],
                    supportedLanguageScriptableObject.supportedLanguages[i].ToString()));
            }
        }

        private void ModifyKey(int key)
        {
            LocalisationModifireEditor.ShowWindow(key);
        }
        private SupportedLanguage GetSelectedLanguage(int value)
        {
            var count = options.Count;
            for (int i = 0; i < count; i++)
            {
                if (i == value)
                    return options[i];
            }
            return SupportedLanguage.None;
        }
        
        private void HighLightObject(ObjectInfo objectInfo)
        {
            objectInfo.unityObjectData.HighLight();
        }
        private void ThreadSearch()
        {
            searchedLanguage = new List<int>();
            bool searchInAll = threadSelectedLanguage == SupportedLanguage.None;
            var count = 0;
            foreach (var item in _allLanguagesMeta)
            {
                count++;
                var data = item.Value;
                for (var i = 0; i < data.localMetaDatas.Count; i++)
                {
                    if (searchInAll)
                    {
                        var str = threadCaseSensitive ? data.localMetaDatas[i].value : data.localMetaDatas[i].value.ToUpper();
                        var sch = threadCaseSensitive ? threadSearchItem : threadSearchItem.ToUpper();
                        if (str.Contains(sch))
                        {
                            searchedLanguage.Add(item.Key);
                            searchCount++;
                            break;
                        }
                    }
                    else if (data.localMetaDatas[i].supportedLanguage == threadSelectedLanguage)
                    {
                        var str = threadCaseSensitive ? data.localMetaDatas[i].value : data.localMetaDatas[i].value.ToUpper();
                        var sch = threadCaseSensitive ? threadSearchItem : threadSearchItem.ToUpper();
                        if (str.Contains(sch))
                        {
                            searchedLanguage.Add(item.Key);
                            searchCount++;
                            break;
                        }
                    }
                }

                searchProgress = (float)totalcount / (float)totalcount;
            }

            threadSearching.Abort();
            
        }

        private void OnDestroy()
        {
            if(threadSearching != null && threadSearching.IsAlive)
                threadSearching.Abort();
        }
    }
}