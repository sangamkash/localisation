﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MagnasStudio.Localisation;
using MagnasStudio.Localisation.Constant;
using MagnasStudio.Util;
using MagnasStudio.Util.Editors;
using Newtonsoft.Json;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MagnasStudio.Editors.Localization
{
    [ConfigEditor("Localization", Priority = 100)]
    public class LocalisationWindowEditor : ConfigEditor
    {
        private static MagnasLocalisationData _Instance;
        private Dictionary<int, CompleteMetaData> _allLanguagesMeta
        {
            get { return GetMagnasLocalisationData._allLanguagesMeta; }
            set { GetMagnasLocalisationData._allLanguagesMeta = value; }
        }
        private List<SceneAsset> sceneAssets
        {
            get { return GetMagnasLocalisationData.sceneAssets; }
            set { GetMagnasLocalisationData.sceneAssets = value; }
        }
        private int totalcount
        {
            get { return GetMagnasLocalisationData.totalcount; }
            set { GetMagnasLocalisationData.totalcount = value; }
        }
        private int totalPrefabChanges
        {
            get { return GetMagnasLocalisationData.totalPrefabChanges; }
            set { GetMagnasLocalisationData.totalPrefabChanges = value; }
        }
        private int key
        {
            get { return GetMagnasLocalisationData.key; }
            set { GetMagnasLocalisationData.key = value; }
        }
        private static string _prefabPath
        {
            get { return GetMagnasLocalisationData._prefabPath; }
            set { GetMagnasLocalisationData._prefabPath = value; }
        }
        private static string _csvPath
        {
            get { return GetMagnasLocalisationData._csvPath; }
            set { GetMagnasLocalisationData._csvPath = value; }
        }
        private static string _LanguageConfigurePath
        {
            get { return GetMagnasLocalisationData._LanguageConfigurePath; }
            set { GetMagnasLocalisationData._LanguageConfigurePath = value; }
        }
        private string LocalisationDataPath => LocalisationConstants.EditorLocalResourceDataPath; 
        private string LocalisationfileName => LocalisationConstants.EditorFileName;
        private const string LOCALISATION_FILE_NAME = LocalisationConstants.LocalisationCSVfileName;
        public static MagnasLocalisationData GetMagnasLocalisationData => MagnasLocalisationData.Instance;
        public static void SaveData() => GetMagnasLocalisationData.Save();
        private SupportedLanguageScriptableObject _supportedLanguageScriptableObject;
        private Rect pwindowSize=>MaganasStudioExtensionWindow.Instance.pwindowSize;
        private string excelFileLocation;
        //contants
        private const string NEXTLINE = "\n";
        private const string SUB_PART = "->";
        private const char QUOTATION = '"';
        private const string DOUBLE_QUOTATION = "\"\"";
        private const string COMMA = ",";
        private const char POINT_SYSBOL = '•';
        private const string PREFAB = "t:Prefab";
        
        public static bool subs = false;
        public static bool openingScene = false;
        public static Action<Scene> OnSceneOpened;
        private bool showCSVFile = false;

        private void OnEnable()
        {
            subs = false;
            openingScene = false;
            if (_LanguageConfigurePath != "")
            {
                var data = AssetDatabase.LoadAssetAtPath<SupportedLanguageScriptableObject>(_LanguageConfigurePath);
                if (data != null)
                {
                   _supportedLanguageScriptableObject = data;
                }
            }
        }

        protected void OnGUI()
        {
            GUILayout.Label("Suppoted Languaged", EditorStyles.boldLabel);
            //=================================================== Localisation configure ===========================================================================
            EditorGUILayout.BeginHorizontal();
            var w = pwindowSize.width-20f;
            var layoutOptionsHalfWScreen = RerturnGUILayoutOption(w / 2, 50);
            var layoutOptionsFullWScreen = RerturnGUILayoutOption(w, 50);
            _supportedLanguageScriptableObject = (SupportedLanguageScriptableObject) EditorGUILayout.ObjectField(
                _supportedLanguageScriptableObject, typeof(SupportedLanguageScriptableObject), false,
                layoutOptionsHalfWScreen);
            if (GUILayout.Button("Set Supported configuration", layoutOptionsHalfWScreen))
            {
                if (_supportedLanguageScriptableObject != null)
                {
                    _LanguageConfigurePath = AssetDatabase.GetAssetPath(_supportedLanguageScriptableObject);
                    SaveData();
                    Debug.Log("<color=green> supportedLanguage is assigned </color>");
                }
            }

            EditorGUILayout.EndHorizontal();
            //=================================================== Localisation configure ===========================================================================

            GUILayout.Label("Add CSV file location", EditorStyles.boldLabel);
            //=================================================== Localisation CSV File Location ===========================================================================

            EditorGUILayout.BeginHorizontal();
            //_localisationCsv = (TextAsset) EditorGUILayout.ObjectField(_localisationCsv, typeof(TextAsset), true,layoutOptionsHalfWScreen);

            _csvPath = EditorGUILayout.TextField(_csvPath, layoutOptionsHalfWScreen);
            if (GUILayout.Button("Set CSV", layoutOptionsHalfWScreen))
            {
                _csvPath = EditorUtility.OpenFolderPanel("", _csvPath, "");
                SaveData();
            }

            EditorGUILayout.EndHorizontal();
            //=================================================== Localisation CSV File Location ===========================================================================
            GUILayout.Label("Set Perfab Path (inside project)", EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal();
            _prefabPath = EditorGUILayout.TextField(_prefabPath, layoutOptionsHalfWScreen);
            if (GUILayout.Button("Set_Prefab_path", layoutOptionsHalfWScreen))
            {
                _prefabPath = EditorUtility.OpenFolderPanel("", _prefabPath, "");
                SaveData();
            }

            EditorGUILayout.EndHorizontal();
            if (GUILayout.Button("Refresh", layoutOptionsFullWScreen))
            {
                RefreshWholeProject();
            }

            if (GUILayout.Button("WriteJson", layoutOptionsFullWScreen))
            {
                WriteLocalisationJSON();
            }

            if (GUILayout.Button("Set scene", layoutOptionsFullWScreen))
            {
                LocalisationAssignSceneEditor.ShowWindow();
            }

            if (GUILayout.Button("Set excel file", layoutOptionsFullWScreen))
            {
                excelFileLocation = EditorUtility.OpenFilePanel("", excelFileLocation, "csv");
                UpdateExcelSheet(excelFileLocation);
            }

            if (GUILayout.Button("View Localisation", layoutOptionsFullWScreen))
            {
                LocalisationViewEditor.ShowWindow(_supportedLanguageScriptableObject);
            }
        }
        
        protected void OnFooterGUI()
        {
            GUILayout.Space(5);
            EditorGUILayout.BeginHorizontal();

            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Refresh", GUILayout.Width(80), GUILayout.Height(30)))
            {
                RefreshWholeProject();
            }
            GUILayout.Space(10);
            if (GUILayout.Button("Push", GUILayout.Width(80), GUILayout.Height(30)))
            {
                
            }
            GUILayout.Space(10);
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(3);
        }

        protected override void LastUpdate()
        {
            base.LastUpdate();
            if (showCSVFile)
            {
                showCSVFile = false;
                ShowCSVFile();
            }
        }

        private GUILayoutOption[] RerturnGUILayoutOption(float w, float h)
        {
            var layoutOptions = new System.Collections.Generic.List<GUILayoutOption>();
            layoutOptions.Add(GUILayout.Width(w));
            layoutOptions.Add(GUILayout.Height(h));

            return layoutOptions.ToArray();
        }

        private void WriteLocalisationJSON()
        {
            var path = Application.dataPath + LocalisationDataPath;
            var directoryPath = path.Replace("/", "\\");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(directoryPath);
            if (_supportedLanguageScriptableObject.ValidateData() == false)
            {
                return;
            }

            var count = _supportedLanguageScriptableObject.supportedLanguages.Count;
            Dictionary<int, string>[] allLanguageList = new Dictionary<int, string>[count];
            string[] fileNameList = new string[count];

            Debug.Log($"count === {count}");
            foreach (var item in _allLanguagesMeta)
            {
                var length = item.Value.localMetaDatas.Count;
                for (var i = 0; i < length; i++)
                {
                    Debug.Log($" supportedLanguage==  {item.Value.localMetaDatas[i].supportedLanguage}");
                }
            }


            foreach (var item in _allLanguagesMeta)
            {
                var count1 = item.Value.localMetaDatas.Count;
                var offset = 0;
                for (var i = offset; i < count1; i++)
                {
                    if (allLanguageList[i - offset] == null)
                    {
                        fileNameList[i - offset] = item.Value.localMetaDatas[i].supportedLanguage.ToString();
                        allLanguageList[i - offset] = new Dictionary<int, string>();
                    }

                    allLanguageList[i - offset].Add(item.Key, item.Value.localMetaDatas[i].value);
                }
            }

            for (int i = 0; i < count; i++)
            {
                var filepath = path + "/" + fileNameList[i] + LocalisationConstants.extension;
                Debug.Log(filepath);
                filepath.WriteFile(JsonConvert.SerializeObject(allLanguageList[i], Formatting.Indented));
            }

        }

        private void GetAllLocalValueFromSceneAndPrefab()
        {
            var listOfScene = FindAssetsByType<SceneAsset>();
            var listOfPrefab = GetAllPrefab();
            var count = listOfPrefab.Count;
            for (int i = 0; i < count; i++)
            {
                //Debug.Log(listOfPrefab[i].name);
                var localstings = PrefabUtility.GetAddedComponents(listOfPrefab[i].prefab);
                var subCount = localstings.Count;
                for (int j = 0; j < subCount; j++)
                {
                    var localbase = localstings[i]?.GetAssetObject() as LocalBase;
                    if (localbase)
                        Debug.Log(localbase.GetLocalValue().value);
                }
            }
        }

        private void ResetLocalisationData()
        {
            totalcount = 0;
            key = 0;
            totalPrefabChanges = 0;
            _allLanguagesMeta = new Dictionary<int, CompleteMetaData>();
        }

        private void RefreshWholeProject()
        {

            if (_supportedLanguageScriptableObject == null)
            {
                Debug.LogError("please set supported language first");
                return;
            }

            if (_supportedLanguageScriptableObject.ValidateData() == false)
            {

                return;
            }

            Debug.Log("<color=green> Refresh whole project Supported Language Scriptable Object validate</color>");
            LocalisationProcessStatusEditor.ShowWindow();
            
            ResetLocalisationData();
            
            LocalisationProcessStatusEditor.window.UpdateProgress(0, "", 1, 3);
            
            UpdateLocalValueInAllPrefab();
            LocalisationProcessStatusEditor.window.UpdateProgress(0, "", 2, 3);
            var scenechanges = UpdateLocalValueInAllScene();
            CreatAndUpdatePrefab();
            LocalisationProcessStatusEditor.window.UpdateProgress(0, "", 3, 3);
            
            var newAllLanguagesMeta = new Dictionary<int, CompleteMetaData>(_allLanguagesMeta);
            CreateCSVFromData(_supportedLanguageScriptableObject, newAllLanguagesMeta);
            Debug.Log("<color=green>===========================Localisation done==========================</color>");
            
            LocalisationProcessReportEditor.ShowWindow(totalcount, totalPrefabChanges, scenechanges);
        }

        private void UpdateLocalValueInAllPrefab()
        {
            string[] guids = AssetDatabase.FindAssets(PREFAB);
            var prefabList = new List<GameObject>();
            var prefabCount = guids.Length;
            for (int i = 0; i < prefabCount; i++)
            {
                string location = AssetDatabase.GUIDToAssetPath(guids[i]);
                var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(location);
                if (prefab != null)
                {
                    bool[] hasChanges = new bool[3];
                    hasChanges[0] = CheckForLocalBase<LocalBase>(location, prefab, prefab.name, ObjectType.Prefab);
                    hasChanges[1] = CheckForLocal_ID_Int<Local_ID_Int>(location, prefab, prefab.name, ObjectType.Prefab);
                    hasChanges[2] = CheckForLocal_ID_Int<Local_ID_String>(location, prefab, prefab.name, ObjectType.Prefab);
                    if (OrAllBool(hasChanges))
                    {
                        totalPrefabChanges++;
                        PrefabUtility.SavePrefabAsset(prefab);
                    }

                    prefabList.Add(prefab);
                }

                LocalisationProcessStatusEditor.window?.UpdateProgress(i / (float) prefabCount, location, 1, 3);
            }
        }

        private string UpdateLocalValueInAllScene()
        {
            string changes = string.Empty;
            if (sceneAssets == null)
            {
                Debug.LogWarning("No scene is added for localisation");
                return changes;
            }

            var length = sceneAssets.Count;
            if (length == 0)
            {
                Debug.LogWarning("No scene is added for localisation");
                return changes;
            }
            for (int i = 0; i < length; i++)
            {
                if (sceneAssets[i] == null)
                    continue;
                string path = AssetDatabase.GetAssetPath(sceneAssets[i]);
                OpenScene(path, null);
                if (UpdateLocalStringInScene(path, sceneAssets[i].name))
                    changes += path + NEXTLINE;
            }

            return changes;
        }

        private bool UpdateLocalStringInScene(string location, string scenename)
        {
            bool[] hasChanges = new bool[3];
            hasChanges[0] = CheckForLocalBaseInScene<LocalBase>(location, scenename);
            hasChanges[1] = CheckForLocal_ID_IntInScene<Local_ID_Int>(location, scenename);
            hasChanges[2] = CheckForLocal_ID_IntInScene<Local_ID_String>(location, scenename);
            if (OrAllBool(hasChanges))
            {
                var scene = EditorSceneManager.GetActiveScene();
                Debug.Log($"<color=green>saving scene {scene.name} </color>");
                EditorSceneManager.SaveScene(scene);
                return true;
            }

            return false;
        }
        
        #region Update All scene

        private void OnDestroy()
        {
            //customWindow = null;
            SaveData();
            if (subs == true)
            {
                EditorSceneManager.sceneOpened -= OnSceneOpended;
                subs = false;
            }
        }

        public static void OpenScene(string path, Action<Scene> OnSceneLoaded)
        {
            if (openingScene == true)
            {
                Debug.Log("Scene is in loading state");
                return;
            }

            if (subs == false)
            {
                subs = true;
                EditorSceneManager.sceneOpened += OnSceneOpended;
            }

            openingScene = true;
            EditorSceneManager.OpenScene(path);
        }

        static void OnSceneOpended(Scene scene, UnityEditor.SceneManagement.OpenSceneMode mode)
        {
            openingScene = false;
            Debug.Log("scene opened " + scene.name);
            if (OnSceneOpened != null)
            {
                OnSceneOpened(scene);
            }
        }

        #endregion

        private void CreatAndUpdatePrefab()
        {
            GameObject gameobj = new GameObject("LocalisationManager");
            var localisationManager = gameobj.AddComponent<LocalisationManager>();
            var finalPath = FinalPath(_prefabPath);
            Debug.Log(finalPath + "LocalisationManager.prefab");
            PrefabUtility.SaveAsPrefabAsset(gameobj, finalPath + "LocalisationManager.prefab");
            GameObject.DestroyImmediate(gameobj);
        }

        private bool AlreadHasString(string value, out int key, SupportedLanguage supportedLanguage)
        {
            foreach (var item in _allLanguagesMeta)
            {
                var count = item.Value.localMetaDatas.Count;
                for (int i = 0; i < count; i++)
                {
                    if (item.Value.localMetaDatas[i].supportedLanguage == supportedLanguage &&
                        item.Value.localMetaDatas[i].value == value)
                    {

                        key = item.Key;
                        return true;
                    }
                }
            }

            key = 0;
            return false;
        }

        private void CreateCSVFromData(SupportedLanguageScriptableObject supportedLanguageScriptableObject, Dictionary<int, CompleteMetaData> data)
        {
            if (supportedLanguageScriptableObject == null)
            {
                Debug.LogError("pls set supported language first");
                return;
            }

            var content = "Location,Key";
            var count = _supportedLanguageScriptableObject.supportedLanguages.Count;
            for (int i = 0; i < count; i++)
            {
                content += COMMA + supportedLanguageScriptableObject.supportedLanguages[i].ToString();
            }

            content += NEXTLINE;

            foreach (var item in data)
            {
                // item.Value.localMetaDatas.Insert(0, new LocalMetaData(supportedLanguageScriptableObject.defaultLanguageType,item.Value.GetCombinedPathString()));
                // item.Value.localMetaDatas.Insert(1, new LocalMetaData(supportedLanguageScriptableObject.defaultLanguageType,item.Key.ToString()));
                var length = item.Value.localMetaDatas.Count;
                for (int i = 0; i < length + 2; i++)
                {
                    if (i == 0)
                        content += QUOTATION + ReturnCSVString(item.Value.GetCombinedPathString()) + QUOTATION;
                    else if (i == 1)
                        content += COMMA + QUOTATION + ReturnCSVString(item.Key.ToString()) + QUOTATION;
                    else
                        content += COMMA + QUOTATION + ReturnCSVString(item.Value.localMetaDatas[i - 2].value) +
                                   QUOTATION;
                }

                content += NEXTLINE;
            }

            var filepath = _csvPath + "/" + LOCALISATION_FILE_NAME;
            Debug.Log(filepath);
            var file = File.Open(filepath, FileMode.OpenOrCreate);
            var bytes = Encoding.UTF8.GetBytes(content);
            file.Write(bytes, 0, bytes.Length);
            file.Close();
            showCSVFile = true;
        }

        private void ShowCSVFile()
        {
            System.Diagnostics.Process.Start(_csvPath);
        }
        
        #region Misc function to check

        private bool CheckForLocalBase<T>(string location, GameObject prefab, string name, ObjectType type) where T : LocalBase
        {
            var children = prefab.GetComponentsInChildren<T>();
            var count = children.Length;
            bool haschanges = false;
            for (int i = 0; i < count; i++)
            {
                var data = children[i];
                var objectData = data.gameObject.GetObjectData(type, location);
                haschanges = Haschanges(location, data.GetLocalValue(), haschanges, name, type,objectData);
            }
            return haschanges;
        }

        private bool CheckForLocal_ID_Int<T>(string location, GameObject prefab, string name, ObjectType type) where T : Local_ID_Type
        {
            var list = prefab.GetComponentsInChildren<T>();
            var length = list.Length;
            bool haschanges = false;
            for (int i = 0; i < length; i++)
            {
                var data = list[i];
                var subData = data.GetListOfLocalValue();
                var count = subData.Count;
                for (int j = 0; j < count; j++)
                {
                    var objectData = data.gameObject.GetObjectData(type, location);
                    haschanges = Haschanges(location, subData[i], haschanges, name, type,objectData);
                }
            }

            return haschanges;
        }

        private bool Haschanges(string location, LocalValue data, bool haschanges, string name, ObjectType type,UnityObjectData unityObjectData)
        {
            var localKey = key;
            var localstr = data.value;
            if (AlreadHasString(localstr, out localKey, _supportedLanguageScriptableObject.defaultLanguageType))
            {
                var value = _allLanguagesMeta[localKey];
                value.AddLocation(name, type, location,unityObjectData);
            }
            else
            {
                key++;
                totalcount++;
                localKey = key;
                var value = new List<LocalMetaData>();
                value.Add(new LocalMetaData(_supportedLanguageScriptableObject.defaultLanguageType, localstr));
                var count = _supportedLanguageScriptableObject.supportedLanguages.Count;

                for (var i = 0; i < count; i++)
                {
                    if (_supportedLanguageScriptableObject.supportedLanguages[i] ==
                        _supportedLanguageScriptableObject.defaultLanguageType)
                        continue;
                    value.Add(new LocalMetaData(_supportedLanguageScriptableObject.supportedLanguages[i], ""));
                }

                var completedata = new CompleteMetaData(value);
                completedata.AddLocation(name, type, location,unityObjectData);
                _allLanguagesMeta.Add(localKey, completedata);
            }

            if (data.key != localKey)
            {
                haschanges = true;
                data.key = localKey;
            }

            return haschanges;
        }

        private bool OrAllBool(bool[] boolArray)
        {
            bool value = false;
            for (int i = 0; i < boolArray.Length; i++)
            {
                value = value || boolArray[i];
                if (value == true)
                    return false;
            }

            return value;
        }

        private bool CheckForLocalBaseInScene<T>(string location, string scenename) where T : LocalBase
        {
            var list = FindObjectsOfType<T>();
            var subCount = list.Length;
            bool haschanges = false;
            for (int j = 0; j < subCount; j++)
            {
                var data = list[j];
                var newlocation = location + NEXTLINE + GetGameObjectPathInScene(data.gameObject);
                var objectData = data.gameObject.GetObjectData(ObjectType.SceneObject, location);
                haschanges = Haschanges(newlocation, data.GetLocalValue(), haschanges, scenename,ObjectType.SceneObject,objectData);
            }
            return haschanges;
        }

        private bool CheckForLocal_ID_IntInScene<T>(string location, string scenename) where T : Local_ID_Type
        {
            var list = FindObjectsOfType<T>();
            var length = list.Length;
            bool haschanges = false;
            for (int i = 0; i < length; i++)
            {
                var data = list[i];
                var subData = data.GetListOfLocalValue();
                var count = subData.Count;
                var newlocation = location + NEXTLINE + SUB_PART + GetGameObjectPathInScene(data.gameObject);
                for (int j = 0; j < count; j++)
                {
                    var objectData = data.gameObject.GetObjectData(ObjectType.SceneObject, location);
                    haschanges = Haschanges(newlocation, subData[i], haschanges, scenename, ObjectType.SceneObject,objectData);
                }
            }

            return haschanges;
        }

        private void UpdateExcelSheet(string path)
        {
            string text = System.IO.File.ReadAllText(path);
            Debug.Log(text);
        }

        #endregion

        #region Misc Fucntion To find

        public static List<T> FindAssetsByType<T>() where T : UnityEngine.Object
        {
            List<T> assets = new System.Collections.Generic.List<T>();
            string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
            for (int i = 0; i < guids.Length; i++)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
                T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
                if (asset != null)
                {
                    assets.Add(asset);
                }
            }
            return assets;
        }

        public static List<PrefabData> GetAllPrefab()
        {
            System.Collections.Generic.List<PrefabData> assets = new System.Collections.Generic.List<PrefabData>();
            string[] guids = AssetDatabase.FindAssets(PREFAB);
            for (int i = 0; i < guids.Length; i++)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
                var asset = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
                if (asset != null)
                {
                    PrefabData data = new PrefabData() {prefab = asset, path = assetPath};
                    assets.Add(data);
                }
            }

            return assets;
        }

        private string FinalPath(string paths)
        {
            var tempPath = _prefabPath.Split('/');
            var finalPath = "Assets/";
            int startAddeding = tempPath.Length;
            for (int i = 0; i < tempPath.Length; i++)
            {
                if (tempPath[i] == "Assets")
                    startAddeding = i;
                if (i > startAddeding)
                    finalPath += tempPath[i] + "/";
            }

            return finalPath;
        }

        public static string GetGameObjectPathInScene(GameObject obj)
        {
            string path = "/" + obj.name;
            while (obj.transform.parent != null)
            {
                obj = obj.transform.parent.gameObject;
                path = "/" + obj.name + path;
            }

            return path;
        }

        #endregion

        #region CSV File

        private string ReturnCSVString(string value)
        {
            return value.Replace(QUOTATION.ToString(), DOUBLE_QUOTATION);
        }

        #endregion
    }
}
