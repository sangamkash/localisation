﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
//using Boo.Lang;
using UnityEditor;
using UnityEngine;
using Newtonsoft.Json;
using Object = UnityEngine.Object;
using MagnasStudio.Localisation.Constant;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace MagnasStudio.Editors.Localization
{
    public class LocalisationProcessStatusEditor : EditorWindow
    {
        public static LocalisationProcessStatusEditor window;
        public float progress;
        Vector2 scrollPos;
        private const string nextLine="\n";
        private string progressText;
        private StringBuilder statusTest;
        [MenuItem("Window/MagnasStudio/ProcessStatus")]
        public static void ShowWindow()
        {
            window = GetWindow<LocalisationProcessStatusEditor>("LocalisationStatus");
            window.progressText = "0/0";
            window.position= new Rect(0,0,800,600);
            window.statusTest=new StringBuilder();
        }
        private void OnGUI()
        {
            if(window == null)
                return;
            EditorGUILayout.BeginVertical();
            {
                GUILayout.Label("Processing Status", EditorStyles.boldLabel);
                var w = window.position.width;
                var h = window.position.height;

                EditorGUI.ProgressBar(new Rect(3, 45, position.width - 6, 20), progress, progressText);
                GUILayout.Space(45);
                GUILayout.Label("Processing prefabs", EditorStyles.label);
                scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(w), GUILayout.Height(300));
                {
                    for (int i = 0; i < 10; i++)
                    {
                        GUILayout.Label(statusTest.ToString());
                    }
                }
                EditorGUILayout.EndScrollView();
            }
            EditorGUILayout.EndVertical();
        }
        public void UpdateProgress(float value,string text,int step,int totolStep)
        {
            progress = value;
            statusTest.Append(text + nextLine);
            progressText = step + "/" + totolStep;
        }

    }
    
    
}
