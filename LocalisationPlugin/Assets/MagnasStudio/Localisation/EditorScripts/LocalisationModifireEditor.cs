﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MagnasStudio.Editors.Localization
{
    public class LocalisationModifireEditor : EditorWindow
    {
        private LocalisationModifireEditor customWindow;
        private Vector2 scrollPos;
        private Dictionary<int,CompleteMetaData> _allLanguagesMeta
        {
            get { return MagnasLocalisationData.Instance._allLanguagesMeta; }
        }

        private CompleteMetaData metaData;
        public static void ShowWindow(int key)
        {
            var window= (LocalisationModifireEditor)EditorWindow.GetWindow(typeof(LocalisationModifireEditor));
            window.customWindow = window;
            window.GetMetaData(key);
        }
        
        public void GetMetaData(int key)
        {
            if (_allLanguagesMeta.TryGetValue(key, out metaData) == false)
            {
                Debug.Log($"Connot able to find key {key}");
            }
        }

        void OnGUI()
        {
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(800));
                EditorGUILayout.BeginVertical();
                    foreach (var item in metaData.localMetaDatas)
                    {
                        EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField(item.supportedLanguage.ToString());
                            EditorGUILayout.TextArea(item.value, GUILayout.Width(350), GUILayout.Height(30));
                            if (GUILayout.Button("Update", GUILayout.Width(100),
                                GUILayout.Height(30)))
                            {
                               
                            }
                        EditorGUILayout.EndHorizontal();;
                    }
                EditorGUILayout.EndVertical();
            EditorGUILayout.EndScrollView();
        }
    }
}
