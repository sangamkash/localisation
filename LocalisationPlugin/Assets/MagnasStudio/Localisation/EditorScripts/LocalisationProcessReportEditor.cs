﻿using UnityEditor;
using UnityEngine;

namespace MagnasStudio.Editors.Localization
{
    public class LocalisationProcessReportEditor : EditorWindow
    {
        private static int totalValue;
        private static int totalPrefabChanges;
        private static string sceneChanged;
        private static LocalisationProcessReportEditor window;
        private Vector3 scrollPos;
        public static void ShowWindow(int _totalValue, int _totalPrefabChanges, string sceneChanged)
        {
            window = ScriptableObject.CreateInstance<LocalisationProcessReportEditor>();
            window.position = new Rect(Screen.width / 2, Screen.height / 2, 500, 250);
            window.ShowPopup();
            totalValue = _totalValue;
            totalPrefabChanges = _totalPrefabChanges;
            LocalisationProcessReportEditor.sceneChanged = sceneChanged;
        }

        private void OnGUI()
        {
            EditorGUILayout.BeginVertical();
            GUILayout.Label("Localisation Completed", EditorStyles.boldLabel);
            GUILayout.Label("Total Value" + totalValue);
            GUILayout.Label("Total PrefabChanged" + totalPrefabChanges);
            GUILayout.Label("List of Scene Changes", EditorStyles.boldLabel);
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(500), GUILayout.Height(200));
            GUILayout.Label( sceneChanged);
            if (GUILayout.Button("ok"))
            {
                window.Close();
            }
            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();
        }
    }
}
