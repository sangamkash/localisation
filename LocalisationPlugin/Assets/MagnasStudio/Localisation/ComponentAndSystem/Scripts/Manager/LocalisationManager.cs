﻿using MagnasStudio.Localisation.Constant;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.Serialization;

namespace MagnasStudio.Localisation
{
    public sealed class LocalisationManager : MonoBehaviour
    {
        public static LocalisationManager instance;

        public static LocalisationManager Instance
        {
            get
            {
                if(instance == null)
                    FindObjectOfType<LocalisationManager>()?.Init();
                return instance;
            }
        }

        [SerializeField] private SupportedLanguage defaultLanguageType;
        [SerializeField] private SupportedLanguage currentLanguageType;

        private const string warningMessage = "<color=blue>{0}</color> value not found";
        private Dictionary<int, string> allLocalStrings;
        private LinkedList<ILocal> linkedListLocal;

        private void Init()
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
            linkedListLocal= new LinkedList<ILocal>();
            LoadLanguage(defaultLanguageType);
        }

        private void Start()
        {
            ChangeLanguage(defaultLanguageType);
        }

        private void LoadLanguage(SupportedLanguage supportedLanguage)
        {

            var resourcePath = LocalisationConstants.ResourcePath + supportedLanguage.ToString();// + ".json";
            Debug.Log(resourcePath);
            var jsonData=Resources.Load<TextAsset>(resourcePath);
            if(jsonData == null)
            {
                Debug.LogError("<color=red>cannot find localisation json file</color>");
                return;
            }
            Debug.Log(jsonData.name);
            Debug.Log(jsonData.text);
            allLocalStrings = JsonConvert.DeserializeObject<Dictionary<int, string>>(jsonData.text);
        }
        
        /// <summary>
        /// Don't use it in OnDestory 
        /// </summary>
        public LinkedListNode<ILocal> Subscribe(ILocal local)
        {
            return linkedListLocal.AddFirst(local);
        }
        
        /// <summary>
        /// Don't use it in OnDestory 
        /// </summary>
        public void UnSubscribe(LinkedListNode<ILocal> local)
        {
            if (local != null)
                linkedListLocal.Remove(local);
        }

        public string GetLocalisedValue(int id)
        {
            string value = null;
            if (allLocalStrings.TryGetValue(id, out value))
            {
                var index = (int) currentLanguageType;
                if (value.Length > index)
                    return value;
            }
            return string.Format(warningMessage, id);
        }

        public void ChangeLanguage(SupportedLanguage languageType)
        {
            currentLanguageType = languageType;
            LoadLanguage(currentLanguageType);
            foreach (var local in linkedListLocal)
            {
                local?.OnLanguageChange(languageType);
            }
        }
        
    }
}
