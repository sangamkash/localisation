﻿
namespace MagnasStudio.Localisation.Constant
{
    [System.Serializable]
    public enum SupportedLanguage
    {
        None,
        Afrikaans,
        Albanian,
        Amharic,
        Arabic,
        Armenian,
        Azerbaijani,
        Basque,
        Belarusian,
        Bengali,
        Bosnian,
        Bulgarian,
        Catalan,
        Cebuano,
        Chinese_Simplified,
        Chinese_Traditional,
        Corsican,
        Croatian,
        Czech,
        Danish,
        Dutch,
        English,
        Esperanto,
        Estonian,
        Finnish,
        French,
        Frisian,
        Galician,
        Georgian,
        German,
        Greek,
        Gujarati,
        Haitian_Creole,
        Hausa,
        Hawaiian,
        Hebrew,
        Hindi,
        Hmong,
        Hungarian,
        Icelandic,
        Igbo,
        Indonesian,
        Irish,
        Italian,
        Japanese,
        Javanese,
        Kannada,
        Kazakh,
        Khmer,
        Korean,
        Kurdish,
        Kyrgyz,
        Lao,
        Latin,
        Latvian,
        Lithuanian,
        Luxembourgish,
        Macedonian,
        Malagasy,
        Malay,
        Malayalam,
        Maltese,
        Maori,
        Marathi,
        Mongolian,
        Myanmar_Burmese,
        Nepali,
        Norwegian,
        Nyanja_Chichewa,
        Pashto,
        Persian,
        Polish,
        Portuguese_Portugal_Brazil,
        Punjabi,
        Romanian,
        Russian,
        Samoan,
        ScotsGaelic,
        Serbian,
        Sesotho,
        Shona,
        Sindhi,
        Sinhala_Sinhalese,
        Slovak,
        Slovenian,
        Somali,
        Spanish,
        Sundanese,
        Swahili,
        Swedish,
        Tagalog_Filipino,
        Tajik,
        Tamil,
        Telugu,
        Thai,
        Turkish,
        Ukrainian,
        Urdu,
        Uzbek,
        Vietnamese,
        Welsh,
        Xhosa,
        Yiddish,
        Yoruba,
        Zulu
    }

    public static class SupportedLanguageCode
    {
        public const string None = "none";
        public const string Afrikaans = "af";
        public const string Albanian = "sq";
        public const string Amharic = "am";
        public const string Arabic = "ar";
        public const string Armenian = "hy";
        public const string Azerbaijani = "az";
        public const string Basque = "eu";
        public const string Belarusian = "be";
        public const string Bengali= "bn";
        public const string Bosnian= "bs";
        public const string Bulgarian  = "bg";
        public const string Catalan = "ca";
        public const string Cebuano = "ceb"; //(ISO-639-2)
        public const string Chinese_Simplified = "zh-CN";// or zh(BCP-47)
        public const string Chinese_Traditional = "zh-TW";// (BCP-47)
        public const string Corsican = "co";
        public const string Croatian = "hr";
        public const string Czech= "cs";
        public const string Danish= "da";
        public const string Dutch= "nl";
        public const string English= "en";
        public const string Esperanto= "eo";
        public const string Estonian = "et";
        public const string Finnish = "fi";
        public const string French= "fr";
        public const string Frisian= "fy";
        public const string Galician= "gl";
        public const string Georgian= "ka";
        public const string German= "de";
        public const string Greek= "el";
        public const string Gujarati= "gu";
        public const string Haitian_Creole = "ht";
        public const string Hausa = "ha";
        public const string Hawaiian = "haw";// (ISO-639-2)
        public const string Hebrew = "he";// or iw
        public const string Hindi = "hi";
        public const string Hmong = "hmn";//(ISO-639-2)
        public const string Hungarian = "hu";
        public const string Icelandic = "is";
        public const string Igbo = "ig";
        public const string Indonesian = "id";
        public const string Irish = "ga";
        public const string Italian = "it";
        public const string Japanese = "ja";
        public const string Javanese = "jv";
        public const string Kannada = "kn";
        public const string Kazakh = "kk";
        public const string Khmer = "km";
        public const string Korean = "ko";
        public const string Kurdish = "ku";
        public const string Kyrgyz = "ky";
        public const string Lao = "lo";
        public const string Latin = "la";
        public const string Latvian = "lv";
        public const string Lithuanian = "lt";
        public const string Luxembourgish = "lb";
        public const string Macedonian = "mk";
        public const string Malagasy = "mg";
        public const string Malay = "ms";
        public const string Malayalam = "ml";
        public const string Maltese = "mt";
        public const string Maori = "mi";
        public const string Marathi = "mr";
        public const string Mongolian = "mn";
        public const string Myanmar_Burmese = "my";
        public const string Nepali = "ne";
        public const string Norwegian = "no";
        public const string Nyanja_Chichewa = "ny";
        public const string Pashto = "ps";
        public const string Persian = "fa";
        public const string Polish = "pl";
        public const string Portuguese_Portugal_Brazil = "pt";
        public const string Punjabi = "pa";
        public const string Romanian = "ro";
        public const string Russian = "ru";
        public const string Samoan = "sm";
        public const string ScotsGaelic = "gd";
        public const string Serbian = "sr";
        public const string Sesotho = "st";
        public const string Shona = "sn";
        public const string Sindhi = "sd";
        public const string Sinhala_Sinhalese = "si";
        public const string Slovak = "sk";
        public const string Slovenian = "sl";
        public const string Somali = "so";
        public const string Spanish = "es";
        public const string Sundanese = "su";
        public const string Swahili = "sw";
        public const string Swedish = "sv";
        public const string Tagalog_Filipino = "tl";
        public const string Tajik = "tg";
        public const string Tamil = "ta";
        public const string Telugu = "te";
        public const string Thai = "th";
        public const string Turkish = "tr";
        public const string Ukrainian = "uk";
        public const string Urdu = "ur";
        public const string Uzbek = "uz";
        public const string Vietnamese = "vi";
        public const string Welsh = "cy";
        public const string Xhosa = "xh";
        public const string Yiddish = "yi";
        public const string Yoruba = "yo";
        public const string Zulu = "zu";
    }

    public static class LocalisationConstants
    {
        public const string KeyNoteFind = "{0} not found";
        public const string EditorLocalResourceDataPath = "/LocalisationData/Resources/" + EditorResourcePath;
        public const string EditorResourcePath = "LocalisationJson";
        public const string EditorFileName = LocalisationJSONfileName + extension;
        public const string ResourcePath = EditorResourcePath + "/" ;
        public const string LocalisationJSONfileName = "Localisation";
        public const string extension = ".json";
        public const string LocalisationCSVfileName = "local_File.csv";
        public static string GetLanguageCode(SupportedLanguage language)
        {
            switch (language)
            {
               case SupportedLanguage.None:
                  return SupportedLanguageCode.None;
                case SupportedLanguage.Afrikaans:
                   return SupportedLanguageCode.Afrikaans;
                case SupportedLanguage.Albanian:
                   return SupportedLanguageCode.Albanian;
                case SupportedLanguage.Amharic:
                   return SupportedLanguageCode.Amharic;
                case SupportedLanguage.Arabic:
                   return SupportedLanguageCode.Arabic;
                case SupportedLanguage.Armenian:
                   return SupportedLanguageCode.Armenian;
                case SupportedLanguage.Azerbaijani:
                   return SupportedLanguageCode.Azerbaijani;
                case SupportedLanguage.Basque:
                   return SupportedLanguageCode.Basque;
                case SupportedLanguage.Belarusian:
                   return SupportedLanguageCode.Belarusian;
                case SupportedLanguage.Bengali:
                   return SupportedLanguageCode.Bengali;
                case SupportedLanguage.Bosnian:
                   return SupportedLanguageCode.Bosnian;
                case SupportedLanguage.Bulgarian:
                   return SupportedLanguageCode.Bulgarian;
                case SupportedLanguage.Catalan:
                   return SupportedLanguageCode.Catalan;
                case SupportedLanguage.Cebuano:
                   return SupportedLanguageCode.Cebuano;
                case SupportedLanguage.Chinese_Simplified:
                   return SupportedLanguageCode.Chinese_Simplified;
                case SupportedLanguage.Chinese_Traditional:
                   return SupportedLanguageCode.Chinese_Traditional;
                case SupportedLanguage.Corsican:
                   return SupportedLanguageCode.Corsican;
                case SupportedLanguage.Croatian:
                   return SupportedLanguageCode.Croatian;
                case SupportedLanguage.Czech:
                   return SupportedLanguageCode.Czech;
                case SupportedLanguage.Danish:
                   return SupportedLanguageCode.Danish;
                case SupportedLanguage.Dutch:
                   return SupportedLanguageCode.Dutch;
                case SupportedLanguage.English:
                   return SupportedLanguageCode.English;
                case SupportedLanguage.Esperanto:
                   return SupportedLanguageCode.Esperanto;
                case SupportedLanguage.Estonian:
                   return SupportedLanguageCode.Estonian;
                case SupportedLanguage.Finnish:
                   return SupportedLanguageCode.Finnish;
                case SupportedLanguage.French:
                   return SupportedLanguageCode.French;
                case SupportedLanguage.Frisian:
                   return SupportedLanguageCode.Frisian;
                case SupportedLanguage.Galician:
                   return SupportedLanguageCode.Galician;
                case SupportedLanguage.Georgian:
                   return SupportedLanguageCode.Georgian;
                case SupportedLanguage.German:
                   return SupportedLanguageCode.German;
                case SupportedLanguage.Greek:
                   return SupportedLanguageCode.Albanian;
                case SupportedLanguage.Gujarati:
                   return SupportedLanguageCode.Gujarati;
                case SupportedLanguage.Haitian_Creole:
                   return SupportedLanguageCode.Haitian_Creole;
                case SupportedLanguage.Hausa:
                   return SupportedLanguageCode.Hausa;
                case SupportedLanguage.Hawaiian:
                   return SupportedLanguageCode.Hawaiian;
                case SupportedLanguage.Hebrew:
                   return SupportedLanguageCode.Hebrew;
                case SupportedLanguage.Hindi:
                   return SupportedLanguageCode.Hindi;
                case SupportedLanguage.Hmong:
                   return SupportedLanguageCode.Hmong;
                case SupportedLanguage.Hungarian:
                   return SupportedLanguageCode.Hungarian;
                case SupportedLanguage.Icelandic:
                   return SupportedLanguageCode.Icelandic;
                case SupportedLanguage.Igbo:
                   return SupportedLanguageCode.Igbo;
                case SupportedLanguage.Indonesian:
                   return SupportedLanguageCode.Indonesian;
                case SupportedLanguage.Irish:
                   return SupportedLanguageCode.Irish;
                case SupportedLanguage.Italian:
                   return SupportedLanguageCode.Italian;
                case SupportedLanguage.Japanese:
                   return SupportedLanguageCode.Japanese;
                case SupportedLanguage.Javanese:
                   return SupportedLanguageCode.Javanese;
                case SupportedLanguage.Kannada:
                   return SupportedLanguageCode.Kannada;
                case SupportedLanguage.Kazakh:
                   return SupportedLanguageCode.Kazakh;
                case SupportedLanguage.Khmer:
                   return SupportedLanguageCode.Khmer;
                case SupportedLanguage.Korean:
                   return SupportedLanguageCode.Korean;
                case SupportedLanguage.Kurdish:
                   return SupportedLanguageCode.Kurdish;
                case SupportedLanguage.Kyrgyz:
                   return SupportedLanguageCode.Kyrgyz;
                case SupportedLanguage.Lao:
                   return SupportedLanguageCode.Lao;
                case SupportedLanguage.Latin:
                   return SupportedLanguageCode.Latin;
                case SupportedLanguage.Latvian:
                   return SupportedLanguageCode.Latvian;
                case SupportedLanguage.Lithuanian:
                   return SupportedLanguageCode.Lithuanian;
                case SupportedLanguage.Luxembourgish:
                   return SupportedLanguageCode.Luxembourgish;
                case SupportedLanguage.Macedonian:
                   return SupportedLanguageCode.Macedonian;
                case SupportedLanguage.Malagasy:
                   return SupportedLanguageCode.Malagasy;
                case SupportedLanguage.Malay:
                   return SupportedLanguageCode.Malay;
                case SupportedLanguage.Malayalam:
                   return SupportedLanguageCode.Malayalam;
                case SupportedLanguage.Maltese:
                   return SupportedLanguageCode.Maltese;
                case SupportedLanguage.Maori:
                   return SupportedLanguageCode.Maori;
                case SupportedLanguage.Marathi:
                   return SupportedLanguageCode.Marathi;
                case SupportedLanguage.Mongolian:
                   return SupportedLanguageCode.Mongolian;
                case SupportedLanguage.Myanmar_Burmese:
                   return SupportedLanguageCode.Myanmar_Burmese;
                case SupportedLanguage.Nepali:
                   return SupportedLanguageCode.Nepali;
                case SupportedLanguage.Norwegian:
                   return SupportedLanguageCode.Norwegian;
                case SupportedLanguage.Nyanja_Chichewa:
                   return SupportedLanguageCode.Nyanja_Chichewa;
                case SupportedLanguage.Pashto:
                   return SupportedLanguageCode.Pashto;
                case SupportedLanguage.Persian:
                   return SupportedLanguageCode.Persian;
                case SupportedLanguage.Polish:
                   return SupportedLanguageCode.Polish;
                case SupportedLanguage.Portuguese_Portugal_Brazil:
                   return SupportedLanguageCode.Portuguese_Portugal_Brazil;
                case SupportedLanguage.Punjabi:
                   return SupportedLanguageCode.Punjabi;
                case SupportedLanguage.Romanian:
                   return SupportedLanguageCode.Romanian;
                case SupportedLanguage.Russian:
                   return SupportedLanguageCode.Russian;
                case SupportedLanguage.Samoan:
                   return SupportedLanguageCode.Samoan;
                case SupportedLanguage.ScotsGaelic:
                   return SupportedLanguageCode.ScotsGaelic;
                case SupportedLanguage.Serbian:
                   return SupportedLanguageCode.Serbian;
                case SupportedLanguage.Sesotho:
                   return SupportedLanguageCode.Sesotho;
                case SupportedLanguage.Shona:
                   return SupportedLanguageCode.Albanian;
                case SupportedLanguage.Sindhi:
                   return SupportedLanguageCode.Sindhi;
                case SupportedLanguage.Sinhala_Sinhalese:
                   return SupportedLanguageCode.Sinhala_Sinhalese;
                case SupportedLanguage.Slovak:
                   return SupportedLanguageCode.Slovak;
                case SupportedLanguage.Slovenian:
                   return SupportedLanguageCode.Slovenian;
                case SupportedLanguage.Somali:
                   return SupportedLanguageCode.Somali;
                case SupportedLanguage.Spanish:
                   return SupportedLanguageCode.Spanish;
                case SupportedLanguage.Sundanese:
                   return SupportedLanguageCode.Sundanese;
                case SupportedLanguage.Swahili:
                   return SupportedLanguageCode.Swahili;
                case SupportedLanguage.Swedish:
                   return SupportedLanguageCode.Swedish;
                case SupportedLanguage.Tagalog_Filipino:
                   return SupportedLanguageCode.Tagalog_Filipino;
                case SupportedLanguage.Tajik:
                   return SupportedLanguageCode.Tajik;
                case SupportedLanguage.Tamil:
                   return SupportedLanguageCode.Tamil;
                case SupportedLanguage.Telugu:
                   return SupportedLanguageCode.Albanian;
                case SupportedLanguage.Thai:
                   return SupportedLanguageCode.Thai;
                case SupportedLanguage.Turkish:
                   return SupportedLanguageCode.Turkish;
                case SupportedLanguage.Ukrainian:
                   return SupportedLanguageCode.Ukrainian;
                case SupportedLanguage.Urdu:
                   return SupportedLanguageCode.Urdu;
                case SupportedLanguage.Uzbek:
                   return SupportedLanguageCode.Uzbek;
                case SupportedLanguage.Vietnamese:
                   return SupportedLanguageCode.Vietnamese;
                case SupportedLanguage.Welsh:
                   return SupportedLanguageCode.Welsh;
                case SupportedLanguage.Xhosa:
                   return SupportedLanguageCode.Xhosa;
                case SupportedLanguage.Yiddish:
                   return SupportedLanguageCode.Yiddish;
                case SupportedLanguage.Yoruba:
                   return SupportedLanguageCode.Yoruba;
                case SupportedLanguage.Zulu:
                   return SupportedLanguageCode.Zulu;
            }
            return "";
        }
    }
    
    #if UNITY_EDITOR
   public static class ObjectMetaType
   {
      public const string SCENE = "scene";
      public const string PREFAB = "prefab";
      public const string SCRIPTABLEOBJECT = "scriptableObject";
   }
   #endif
}
