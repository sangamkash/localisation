﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using MagnasStudio.Localisation.Constant;
using UnityEditor;

namespace MagnasStudio.Localisation
{
    [AddComponentMenu("MagnasStudio/Localisation/Local_TextMeshPro")]
    [RequireComponent(typeof(TextMeshPro))]
    public class LocalTextMeshPro : LocalBase
    {
        private TextMeshPro text;

        protected override void OnEnable()
        {
            base.OnEnable();
            text.text = localValue.ReturnLocalisedValue();
        }

        public override void OnLanguageChange(SupportedLanguage languageType)
        {
            base.OnLanguageChange(languageType);
            text.text = localValue.ReturnLocalisedValue();
        }

        private void OnValidate()
        {
            text = GetComponent<TextMeshPro>();
        }

        public override void SetArabicFixedString()
        {
#if UNITY_EDITOR
            var localString = localValue.value;
            if (EditorApplication.isPlaying )
                localString = localValue.value;
#else
             var localString = localValue.ReturnLocalisedValue();
#endif
            var ShowTashkeel = true;
            var UseHinduNumbers = true;
            if (!string.IsNullOrEmpty(localString))
            {
                string rtlText = ArabicSupport.ArabicFixer.Fix(localString, ShowTashkeel, UseHinduNumbers);
                text.text = rtlText;
                text.isRightToLeftText = true;
            }
            else if(text)
                text.text = "";
        }
#if UNITY_EDITOR
        public void GetButton()
        {
            localValue.value = text.text;
        }
        public void SetButton()
        {
            text.text = localValue.value;
        }
#endif
    }
}