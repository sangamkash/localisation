﻿using System;
using MagnasStudio.Localisation.Constant;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

 namespace MagnasStudio.Localisation
{
    [AddComponentMenu("MagnasStudio/Localisation/3D_Text")]
    [RequireComponent(typeof(TextMesh))]
    public class LocalTextMesh : LocalBase
    {
        private TextMesh text;

        protected override void OnEnable()
        {
            base.OnEnable();
            text.text = localValue.ReturnLocalisedValue();
        }

        public override void OnLanguageChange(SupportedLanguage languageType)
        {
            base.OnLanguageChange(languageType);
            text.text = localValue.ReturnLocalisedValue();
        }

        private void OnValidate()
        {
            text = GetComponent<TextMesh>();
        }
        public override void SetArabicFixedString()
        {
#if UNITY_EDITOR
            var localString = localValue.value;
            if (EditorApplication.isPlaying )
                localString = localValue.value;
#else
             var localString = localValue.ReturnLocalisedValue();
#endif
            var ShowTashkeel = true;
            var UseHinduNumbers = true;
            if (!string.IsNullOrEmpty(localString))
            {
                string rtlText = ArabicSupport.ArabicFixer.Fix(localString, ShowTashkeel, UseHinduNumbers);
                rtlText = rtlText.Replace("\r", "");
                text.text = rtlText;
            }
            else if(text)
                text.text = "";
        }
#if UNITY_EDITOR
        public void GetButton()
        {
            localValue.value = text.text;
        }
        public void SetButton()
        {
            text.text = localValue.value;
        }
#endif
    }
}
