﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagnasStudio.Localisation
{
    public abstract class Local_ID_Type : MonoBehaviour
    {
#if UNITY_EDITOR
       public abstract List<LocalValue> GetListOfLocalValue();
#endif
    }
}
