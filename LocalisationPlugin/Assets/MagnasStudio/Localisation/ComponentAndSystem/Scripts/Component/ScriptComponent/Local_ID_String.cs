﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagnasStudio.Localisation
{
    [AddComponentMenu("MagnasStudio/Localisation/Local_Script_ID_String")]
    public class Local_ID_String : Local_ID_Type
    {
        [SerializeField]
        private LocalValueCustomIdString localValueCustomIdString;

        public string ReturnLocalisedValue(string id)
        {
            return localValueCustomIdString.ReturnLocalisedValue(id);
        }

#if UNITY_EDITOR
        public override List<LocalValue> GetListOfLocalValue()
        {
            return localValueCustomIdString.GetListOfLocalValue();
        }
#endif
    }
}