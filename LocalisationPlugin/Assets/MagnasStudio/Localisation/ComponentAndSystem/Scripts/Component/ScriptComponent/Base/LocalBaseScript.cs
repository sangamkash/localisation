﻿using MagnasStudio.Localisation.Constant;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagnasStudio.Localisation
{
    [System.Serializable]
    public class LocalValueCustomIdInt : ISerializationCallbackReceiver
    {
        [System.Serializable]
        public class LocalData
        {
            public int id;
            public LocalValue localValue;
        }
        public List<LocalData> localDataList;
        public Dictionary<int, LocalValue> localValues;
       
        public string ReturnLocalisedValue(int id)
        {
            LocalValue data;
            if (localValues.TryGetValue(id, out data))
            {
                return data.ReturnLocalisedValue();
            }
            return string.Format(LocalisationConstants.KeyNoteFind, id);
        }

        public void OnAfterDeserialize()
        {
            int count = localDataList.Count;
            localValues = new Dictionary<int, LocalValue>();
            for (int i = 0; i < count; i++)
            {
                try
                {
                    localValues.Add(localDataList[i].id, localDataList[i].localValue);
                }
                catch (Exception ex)
                {
                    Debug.LogError(ex.StackTrace);
                }
            }
        }

        public void OnBeforeSerialize()
        {

        }

#if UNITY_EDITOR
        public List<LocalValue> GetListOfLocalValue()
        {
            List<LocalValue> list = new List<LocalValue>();
            int count = localDataList.Count;
            for (int i = 0; i < count; i++)
            {
                list.Add(localDataList[i].localValue);
            }
            return list;
        }
#endif
    }

    [System.Serializable]
    public class LocalValueCustomIdString : ISerializationCallbackReceiver
    {
        [System.Serializable]
        public class LocalData
        {
            public string id;
            public LocalValue localValue;
        }
        public List<LocalData> localDataList;
        public Dictionary<string, LocalValue> localValues;

        public void OnAfterDeserialize()
        {
            int count = localDataList.Count;
            localValues = new Dictionary<string, LocalValue>();
            for (int i = 0; i < count; i++)
            {
                try
                {
                    localValues.Add(localDataList[i].id, localDataList[i].localValue);
                }
                catch (Exception ex)
                {
                    Debug.LogError(ex.StackTrace);
                }
            }
        }

        public void OnBeforeSerialize()
        {

        }

        public string ReturnLocalisedValue(string id)
        {
            LocalValue data;
            if (localValues.TryGetValue(id, out data))
            {
                return data.ReturnLocalisedValue();
            }
            return string.Format(LocalisationConstants.KeyNoteFind, id);
        }

#if UNITY_EDITOR
        public List<LocalValue> GetListOfLocalValue()
        {
            List<LocalValue> list = new List<LocalValue>();
            int count = localDataList.Count;
            for (int i = 0; i < count; i++)
            {
                list.Add(localDataList[i].localValue);
            }
            return list;
        }
#endif
    }
}