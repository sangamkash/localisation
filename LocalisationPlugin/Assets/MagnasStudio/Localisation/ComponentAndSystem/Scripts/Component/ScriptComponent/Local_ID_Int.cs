﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagnasStudio.Localisation
{
    [AddComponentMenu("MagnasStudio/Localisation/Local_Script_ID_Int")]
    public class Local_ID_Int : Local_ID_Type
    {
        [SerializeField]
        private LocalValueCustomIdInt localValueCustomIdInt;

        public string ReturnLocalisedValue(int id)
        {
            return localValueCustomIdInt.ReturnLocalisedValue(id);
        }


#if UNITY_EDITOR
        public override List<LocalValue> GetListOfLocalValue()
        {
           return localValueCustomIdInt.GetListOfLocalValue();
        }
#endif
    }
}
