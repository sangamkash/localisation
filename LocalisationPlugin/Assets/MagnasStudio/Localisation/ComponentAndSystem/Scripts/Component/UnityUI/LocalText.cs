﻿using System;
using UnityEngine.UI;
using UnityEngine;
using MagnasStudio.Localisation.Constant;
using UnityEditor;

namespace MagnasStudio.Localisation
{
    [AddComponentMenu("MagnasStudio/Localisation/Local_Text")]
    [RequireComponent(typeof(Text))]
    public class LocalText : LocalBase
    {
        private Text text;
        public override void OnLanguageChange(SupportedLanguage languageType)
        {
            base.OnLanguageChange(languageType);
            if (languageType == SupportedLanguage.Arabic)
            {
                SetArabicFixedString();
                return;
            }
            text.text = localValue.ReturnLocalisedValue();
        }

        private void OnValidate()
        {
            text = GetComponent<Text>();
        }

        public override void SetArabicFixedString()
        {
#if UNITY_EDITOR
            var localString = localValue.value;
            if (EditorApplication.isPlaying )
                 localString = localValue.value;
            #else
             var localString = localValue.ReturnLocalisedValue();
#endif
           
            var ShowTashkeel = true;
            var UseHinduNumbers = true;
            if (!string.IsNullOrEmpty(localString))
            {
                string rtlText = ArabicSupport.ArabicFixer.Fix(localString, ShowTashkeel, UseHinduNumbers);
                rtlText = rtlText.Replace("\r", ""); 

                string finalText = "";
                string[] rtlParagraph = rtlText.Split('\n');

                text.text = "";
                for (int lineIndex = 0; lineIndex < rtlParagraph.Length; lineIndex++)
                {
                    string[] words = rtlParagraph[lineIndex].Split(' ');
                    Array.Reverse(words);
                    text.text = string.Join(" ", words);

                    Canvas.ForceUpdateCanvases();
                    for (int i = 0; i < text.cachedTextGenerator.lines.Count; i++)
                    {
                        int startIndex = text.cachedTextGenerator.lines[i].startCharIdx;
                        int endIndex = (i == text.cachedTextGenerator.lines.Count - 1) ? text.text.Length
                             : text.cachedTextGenerator.lines[i + 1].startCharIdx;
                        int length = endIndex - startIndex;

                        string[] lineWords = text.text.Substring(startIndex, length).Split(' ');
                        Array.Reverse(lineWords);

                        finalText = finalText + string.Join(" ", lineWords).Trim() + "\n";
                    }
                 }
                 text.text = finalText.TrimEnd('\n');
             }
             else if(text)
                 text.text = "";
        }
            
        
#if UNITY_EDITOR
        public void GetButton()
        {
            localValue.value = text.text;
        }
        public void SetButton()
        {
            text.text = localValue.value;
        }
 #endif
        
    }
}
