﻿using MagnasStudio.Localisation.Constant;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.VersionControl;
using UnityEngine;

namespace MagnasStudio.Localisation
{ 
    public interface ILocal
    {
        void OnLanguageChange(SupportedLanguage languageType);
    }
}
