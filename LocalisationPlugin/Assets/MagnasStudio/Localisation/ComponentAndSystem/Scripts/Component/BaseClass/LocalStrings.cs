﻿using MagnasStudio.Localisation.Constant;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using UnityEngine;

namespace MagnasStudio.Localisation
{
    [System.Serializable]
    public class LocalValue
    {
        [HideInInspector]
        public int key;
#if UNITY_EDITOR
        public string value = "Hello {0} how are you";
       
#endif
        public string ReturnLocalisedValue()
        {
            return LocalisationManager.Instance.GetLocalisedValue(key);
        }
    }
}
