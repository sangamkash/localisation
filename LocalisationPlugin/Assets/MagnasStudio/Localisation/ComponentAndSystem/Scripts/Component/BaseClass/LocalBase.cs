﻿using MagnasStudio.Localisation.Constant;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagnasStudio.Localisation
{
    public abstract class LocalBase : MonoBehaviour,ILocal
    {
        [SerializeField] protected LocalValue localValue;
        private LinkedListNode<ILocal> node;

        private void Awake()
        {
            Debug.Log($"awake is null{LocalisationManager.Instance == null}");
        }

        protected virtual void OnEnable()
        {
            Debug.Log("Time detlta"+Time.deltaTime);
            Debug.Log($"is null{LocalisationManager.Instance == null}");
            node = LocalisationManager.Instance.Subscribe(this);
        }

        protected virtual void OnDisable()
        {
            LocalisationManager.Instance.UnSubscribe(node);
        }
        public virtual void OnLanguageChange(SupportedLanguage languageType)
        {
            
        }
        
        public abstract void SetArabicFixedString();
        
#if UNITY_EDITOR
        public LocalValue GetLocalValue()
        {
            return localValue;
        }
        
#endif
    }
}
