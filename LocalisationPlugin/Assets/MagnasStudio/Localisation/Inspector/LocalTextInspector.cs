﻿using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MagnasStudio.Localisation.Editors
{
    [CustomEditor(typeof(LocalText))]
    public class LocalTextInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GUILayout.BeginHorizontal();
            var obj = (LocalText) target;
            if (GUILayout.Button("Get"))
            {
                obj.GetButton();
            }
            if (GUILayout.Button("set"))
            {
                obj.SetButton();
            }
            if (GUILayout.Button("ArabicFix"))
            {
                obj.SetArabicFixedString();
            }
            GUILayout.EndHorizontal();
        }
    }

    [CustomEditor(typeof(LocalTextMesh))]
    public class LocalTextMeshInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GUILayout.BeginHorizontal();
            var obj = (LocalTextMesh)target;
            if (GUILayout.Button("Get"))
            {
                obj.GetButton();
            }
            if (GUILayout.Button("set"))
            {
                obj.SetButton();
            }
            if (GUILayout.Button("ArabicFix"))
            {
                obj.SetArabicFixedString();
            }
            GUILayout.EndHorizontal();
        }
    }

    [CustomEditor(typeof(LocalTextMeshPro))]
    public class LocalTextMeshProInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GUILayout.BeginHorizontal();
            var obj = (LocalTextMeshPro)target;
            if (GUILayout.Button("Get"))
            {
                obj.GetButton();
            }
            if (GUILayout.Button("set"))
            {
                obj.SetButton();
            }
            if (GUILayout.Button("ArabicFix"))
            {
                obj.SetArabicFixedString();
            }
            GUILayout.EndHorizontal();
        }
    }

    [CustomEditor(typeof(LocalTextMeshProUGUI))]
         public class LocalTextMeshProUGUIInspector : Editor
         {
             public override void OnInspectorGUI()
             {
                 base.OnInspectorGUI();
                 GUILayout.BeginHorizontal();
                 var obj = (LocalTextMeshProUGUI)target;
                 if (GUILayout.Button("Get"))
                 {
                     obj.GetButton();
                 }
                 if (GUILayout.Button("set"))
                 {
                     obj.SetButton();
                 }
                 if (GUILayout.Button("ArabicFix"))
                 {
                     obj.SetArabicFixedString();
                 }
                 GUILayout.EndHorizontal();
             }
         }
    
    [CustomEditor(typeof(SupportedLanguageScriptableObject))]
    public class SupportedLanguageInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GUILayout.BeginHorizontal();
            var obj = (SupportedLanguageScriptableObject) target;
            if (GUILayout.Button("Validate"))
            {
                obj.ValidateData();
            }
            GUILayout.EndHorizontal();
        }
    }
}
