﻿using MagnasStudio.Localisation.Constant;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagnasStudio.Localisation
{
    [CreateAssetMenu(fileName = "LanguageConfiguration", menuName = "MagnasStudio/SupportedLanguage")]
    public class SupportedLanguageScriptableObject : ScriptableObject 
    {
        public SupportedLanguage defaultLanguageType;
        public List<SupportedLanguage> supportedLanguages;

        public SupportedLanguageScriptableObject()
        {
            supportedLanguages = new List<SupportedLanguage>();
            defaultLanguageType  = SupportedLanguage.English;
            supportedLanguages.Add(SupportedLanguage.English);
        }

        public bool ValidateData()
        {
            if (defaultLanguageType == SupportedLanguage.None)
            {
                Debug.Log("None language is not allowed");
                return false;
            }

            var count = supportedLanguages.Count;
            for (int i = 0; i < count; i++)
            {
                if (supportedLanguages[i] == SupportedLanguage.None) 
                {
                    Debug.Log("None language is not allowed");
                    return false;
                }
            }
            supportedLanguages.Insert(0,defaultLanguageType);
             count = supportedLanguages.Count;
             for (var i = 0; i < count; i++)
             {
                
                 for (int j = i+1; j < count; j++)
                 {
                     if (supportedLanguages[i] == supportedLanguages[j])
                     {
                         supportedLanguages.Remove(supportedLanguages[j]);
                         count--;
                         i--;
                         break;
                     }
                 }
             }

             return true;
        }
        [ContextMenu("AddAll")]
        public void AddAllLanguage()
        {
            supportedLanguages = new List<SupportedLanguage>();
            for (var i = 0; i < 104; i++)
            {
                supportedLanguages.Add((SupportedLanguage) i);
            }
        }
    }
}
