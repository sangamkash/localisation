﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MagnasStudio.Localisation;
using MagnasStudio.Localisation.Constant;
using MagnasStudio.Util;
using MagnasStudio.Util.Editors;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace MagnasStudio.Editors.Localization
{
    public static class LocalisationConstant
    {
        public const string JsonFilePath = "/LocalisationData/EditorData";
        public const string FileName = "MagnasLocalisationEditorData.json";
    }
    
    public class PrefabData
    {
        public GameObject prefab;
        public string path;
    }

    [Serializable]
    public class ObjectInfo
    {
        [FormerlySerializedAs("sceneObjectData")] public UnityObjectData unityObjectData;
        public string name;
        public ObjectType type;
        public string location;

        public ObjectInfo()
        {
            
        }
        public ObjectInfo(string name, ObjectType type, string location)
        {
            this.name = name;
            this.type = type;
            this.location = location;
        }
        
        public ObjectInfo(string name, ObjectType type,string location,UnityObjectData unityObjectData)
        {
            this.name = name;
            this.type = type;
            this.unityObjectData = unityObjectData;
            this.location = location;
        }
    }

    [Serializable]
    public class CompleteMetaData
    {
        public List<LocalMetaData> localMetaDatas;
        public List<ObjectInfo> locations;

        public CompleteMetaData()
        {
            
        }
        public CompleteMetaData(List<LocalMetaData> localMetaDatas)
        {
            this.localMetaDatas = localMetaDatas;
        }

        public void AddLocation(string name, ObjectType type, string path)
        {
            if (locations == null)
                locations = new List<ObjectInfo>();
            locations.Add(new ObjectInfo(name, type, path));
        }
        
        public void AddLocation(string name, ObjectType type, string path,UnityObjectData unityObjectData)
        {
            if (locations == null)
                locations = new List<ObjectInfo>();
            locations.Add(new ObjectInfo(name, type, path,unityObjectData));
        }

        public string GetCombinedPathString()
        {
            var str = "";
            var count = locations.Count;
            for (int i = 0; i < count; i++)
            {
                str += locations[i].location + "\n";
            }

            return str;
        }
    }

    [Serializable]
    public class LocalMetaData
    {
        public SupportedLanguage supportedLanguage;
        public string value;

        public LocalMetaData()
        {

        }

        public LocalMetaData(SupportedLanguage supportedLanguage, string value)
        {
            this.supportedLanguage = supportedLanguage;
            this.value = value;
            //locations= new string[];
        }
    }

    [Serializable]
    public class MagnasLocalisationData
    {
        private static MagnasLocalisationData _Instance;

        public static MagnasLocalisationData Instance
        {
            get
            {
                if (_Instance == null)
                {
                    var obj = DeSerializedObject();
                    // if (obj != null)
                    //     Debug.Log("<color=red>======DeSerializedObject=====</color>");
                    _Instance = obj != null ? obj : new MagnasLocalisationData();
                }

                return _Instance;
            }
        }


        public Dictionary<int, CompleteMetaData> _allLanguagesMeta;
        [NonSerialized] public List<SceneAsset> sceneAssets;
        public List<string> scenePath;

        public string _prefabPath;
        public string _csvPath;
        public string _LanguageConfigurePath;
        public int totalcount = 0;
        public int totalPrefabChanges = 0;
        public int key = 0;
        public bool showMetaData;
       

        private MagnasLocalisationData()
        {
            _allLanguagesMeta = new Dictionary<int, CompleteMetaData>();
            sceneAssets = new List<SceneAsset>();
            scenePath = new List<string>();
            _prefabPath = "";
            _csvPath = "";
            _LanguageConfigurePath = "";
            totalcount = 0;
            totalPrefabChanges = 0;
            key = 0;
            showMetaData = true;
        }

        public void Save()
        {
           
            var path = Application.dataPath + LocalisationConstant.JsonFilePath;
            var directoryPath = path.Replace("/", "\\");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(directoryPath);
            var filepath = path + "/" + LocalisationConstant.FileName;
            filepath.WriteFile(JsonConvert.SerializeObject(this, Formatting.Indented));
            Debug.Log("<color=green>========================Save====================================<green>");
        }

        public static MagnasLocalisationData DeSerializedObject()
        {
            var directory = Application.dataPath + LocalisationConstant.JsonFilePath;
            if (!Directory.Exists(directory))
            {
                Debug.Log("Directory.Exists(directory)");
                return null;
            }

            var filepath = Application.dataPath + LocalisationConstant.JsonFilePath + "/" + LocalisationConstant.FileName;
            if (!File.Exists(filepath))
            {
                Debug.Log("File.Exists(filepath) ||" + Application.dataPath + LocalisationConstant.JsonFilePath + "/" + LocalisationConstant.FileName);
                return null;
            }

            var text = File.ReadAllText(filepath);
            MagnasLocalisationData data = null;
            try
            {
                data = JsonConvert.DeserializeObject<MagnasLocalisationData>(text);
                data.sceneAssets = new List<SceneAsset>();
                var count = data.scenePath.Count;
                for (var i = 0; i < count; i++)
                {
                    var temp = AssetDatabase.LoadAssetAtPath<SceneAsset>(data.scenePath[i]);
                    if (temp != null)
                        data.sceneAssets.Add(temp);
                }

                //Debug.Log("<color=green>==================DeSerializedObject======================</green>");
            }
            catch (Exception ex)
            {
                Debug.LogError("Exception +++++++++++++++++++++++++++++++++++++++++++++++++++++++" + ex.StackTrace);
                return null;
            }

            return data;
        }
    }
}
