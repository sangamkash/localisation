﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEditor.Callbacks;
using System.Reflection;
using UnityEditor.IMGUI.Controls;
using System.Linq;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.UIElements;


namespace  MagnasStudio.Editors
{
    public class MaganasStudioExtensionWindow : EditorWindow
    {
        public static MaganasStudioExtensionWindow Instance;
        private float mLineHeight;
        private string mSearchParameter;
        private SearchField mSearchField;
        private ConfigEditor mActiveEditor;
        private ReorderableList mEditorList;
        private ConfigEditorAttribute[] mEditorAttributes;
        private EditorGUIHorizontalSplitView mHorizontalSplitView;
        private MethodInfo mOnGUIMethodInfo;
        private MethodInfo mOnContextGUIMethodInfo;
        private MethodInfo mOnHeaderGUIMethodInfo;
        private MethodInfo mOnFooterGUIMethodInfo;

        private const string Key_SelectedIndex = "Config_Editor_Index";
        private const string Key_SearchParameter = "Config_Search_Parameter";
        private const string Key_SplitRatio = "Config_Window_SplitRatio";
        public Rect pwindowSize=>mHorizontalSplitView.pRightArea;
        private void OnEnable()
        {
            Instance = this;
            mSearchField = new SearchField();
            mSearchParameter = EditorPrefs.GetString(Key_SearchParameter, string.Empty);
            mLineHeight = EditorGUIUtility.singleLineHeight + 2;
            mHorizontalSplitView = new EditorGUIHorizontalSplitView(new Rect(0, mLineHeight, position.width, position.height - mLineHeight), Key_SplitRatio);

            mEditorAttributes = GetAllConfigEditorAttributes();
            mEditorList = new ReorderableList(mEditorAttributes, typeof(EditorWindow), false, false, false, false);
            mEditorList.index = Mathf.Clamp(EditorPrefs.GetInt(Key_SelectedIndex, 0), 0, mEditorAttributes.Length);
            mEditorList.headerHeight = 0;
            mEditorList.showDefaultBackground = false;
            mEditorList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => GUI.Label(rect, mEditorAttributes[index].Name);
            mEditorList.onSelectCallback = OnEditorSelection;
            mEditorList.onSelectCallback.Invoke(mEditorList);
            Texture iconTexture = this.GetInPathTexture("Gizmos/Icon_"+ (EditorGUIUtility.isProSkin ? "Dark" : "Light") + ".png");
            titleContent = new GUIContent("MagansStudio Extension", iconTexture);
        }

        private void OnGUI()
        {
            DrawToolBar();
            var rec = new Rect(0, mLineHeight, position.width, position.height - mLineHeight);
           
            mHorizontalSplitView.BeginSplitView(new Rect(0, mLineHeight, position.width, position.height - mLineHeight));
            DrawEditorList();
            mHorizontalSplitView.Split();
            DrawActiveEditor();
            mHorizontalSplitView.EndSplitView();
            ProcessInput();
            mActiveEditor?.EndUpdate();
            //Debug.Log($"rec  pRightArea {mHorizontalSplitView.pRightArea.ToString()}  pLeftArea {mHorizontalSplitView.pLeftArea.ToString()}");
        }

        private void OnDisable()
        {
            EditorPrefs.SetInt(Key_SelectedIndex, mEditorList.index);
            TryDestroyActiveEditor();
        }

        private void TryDestroyActiveEditor()
        {
            if (mActiveEditor)
            {
                mActiveEditor.SavePreference();
                DestroyImmediate(mActiveEditor);
            }
        }

        private void ProcessInput()
        {
            ProcessDrag();
        }

        private void ProcessDrag()
        {
            if (Event.current.button == 0 && Event.current.type == EventType.MouseDrag)
            {
                Vector2 screenMousePosition = Event.current.mousePosition;
                RectOffset margin = new RectOffset(-5, -15, -10, -10);
                bool cursorInside = margin.Add(mHorizontalSplitView.pRightArea).Contains(screenMousePosition);
                if (cursorInside)
                {
                    mActiveEditor.ScrollPosition += -Event.current.delta;
                    Event.current.Use();
                }
            }
        }

        private void DrawToolBar()
        {
            GUILayout.BeginHorizontal(EditorStyles.toolbar);
            GUILayout.FlexibleSpace();
            EditorGUI.BeginChangeCheck();
            mSearchParameter = mSearchField?.OnToolbarGUI(mSearchParameter, GUILayout.Width(310));
            if (EditorGUI.EndChangeCheck())
            {
                mActiveEditor.pSearchParameter = mSearchParameter;
                EditorPrefs.SetString(Key_SearchParameter, mSearchParameter);
            }
            GUILayout.EndHorizontal();
        }

        private void DrawEditorList()
        {
            mEditorList?.DoLayoutList();
        }

        private void DrawActiveEditor()
        {
            GUIStyle titleLabel = new GUIStyle(EditorStyles.largeLabel);
            titleLabel.font = EditorStyles.boldFont;
            titleLabel.fontSize = 15;

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label(mActiveEditor.name, titleLabel);
                GUILayout.FlexibleSpace();
                mOnContextGUIMethodInfo?.Invoke(mActiveEditor, null);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginVertical();
            {
                mOnHeaderGUIMethodInfo?.Invoke(mActiveEditor, null);
                mActiveEditor.ScrollPosition = GUILayout.BeginScrollView(mActiveEditor.ScrollPosition);
                {
                    mOnGUIMethodInfo?.Invoke(mActiveEditor, null);
                }
                GUILayout.EndScrollView();
                GUILayout.FlexibleSpace();
                mOnFooterGUIMethodInfo?.Invoke(mActiveEditor, null);
            }
            GUILayout.EndVertical();
        }

        private void OnEditorSelection(ReorderableList list)
        {
            TryDestroyActiveEditor();
            ConfigEditorAttribute configEditorAttribute = mEditorAttributes[list.index];
            Type editorType = configEditorAttribute.Type;
            mActiveEditor = (ConfigEditor)CreateInstance(editorType);
            mActiveEditor.name = configEditorAttribute.Name;
            mActiveEditor.pSearchParameter = mSearchParameter;
            mActiveEditor.LoadPreference();
            mOnGUIMethodInfo = editorType.GetMethod("OnGUI", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            mOnContextGUIMethodInfo = editorType.GetMethod("OnContextGUI", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            mOnHeaderGUIMethodInfo = editorType.GetMethod("OnHeaderGUI", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            mOnFooterGUIMethodInfo = editorType.GetMethod("OnFooterGUI", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        }
        
        private ConfigEditorAttribute[] GetAllConfigEditorAttributes()
        {
        List<ConfigEditorAttribute> attributes = new List<ConfigEditorAttribute>();
        Type configEditorType = typeof(ConfigEditor);
            foreach (Type type in typeof(ConfigEditorAttribute).Assembly.GetTypes())
        {
            ConfigEditorAttribute configEditor = type.GetCustomAttribute<ConfigEditorAttribute>();
            if (configEditor != null && configEditorType.IsAssignableFrom(type))
            {
                configEditor.Type = type;
                attributes.Add(configEditor);
            }
        }
        return attributes.OrderByDescending(e => e.Priority).ToArray();
        }
        
        [MenuItem("Window/MagnasStudio/ExtensionWindow  &l", priority = 300)]
        public static void OpenWindow()
        {
            MaganasStudioExtensionWindow extensionWindow = GetWindow<MaganasStudioExtensionWindow>();
            extensionWindow.position = new Rect(0, 0, 640, 480);
        }

    }
}
