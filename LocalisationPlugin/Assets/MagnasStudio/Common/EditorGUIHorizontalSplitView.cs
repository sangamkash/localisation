﻿using UnityEngine;
using UnityEditor;

namespace MagnasStudio.Editors
{
    public class EditorGUIHorizontalSplitView
    {
        public float ratio;
        protected bool resize;
        [SerializeField] protected Rect area;
        private string Key_SplitRatio;
        public Rect pLeftArea { get; private set; }
        public Rect pRightArea { get; private set; }

        public EditorGUIHorizontalSplitView(Rect area, string key_SplitRatio)
        {
            this.area = area;
            Key_SplitRatio = key_SplitRatio;
            ratio = EditorPrefs.GetFloat(Key_SplitRatio, .3f);
            CalculateDivision(area);
        }

        public EditorGUIHorizontalSplitView(Rect area, float ratio = .3f)
        {
            this.area = area;
            this.ratio = ratio;
            CalculateDivision(area);
        }

        public Rect BeginSplitView(Rect area)
        {
            this.area = area;

            CalculateDivision(area);

            GUILayout.BeginArea(pLeftArea);
            return pLeftArea;
        }

        private void CalculateDivision(Rect area)
        {
            RectOffset margin = new RectOffset(-10, 0, -3, -3);
            Rect leftArea = new Rect(area);
            leftArea.width *= ratio;
            Rect rightArea = new Rect(area);
            rightArea.x = area.width * ratio;
            rightArea.width -= rightArea.x;
            this.pLeftArea = leftArea;
            this.pRightArea = margin.Add(rightArea);
        }

        public Rect Split(bool split = true)
        {
            GUILayout.EndArea();
            ResizeSplitFirstView(true);
            GUILayout.BeginArea(pRightArea);
            return pRightArea;
        }

        public void EndSplitView()
        {
            GUILayout.EndArea();
        }

        private void ResizeSplitFirstView(bool showDivider = false)
        {
            Rect resizeHandleRect = new Rect(area.width * ratio, area.y, 4f, area.height);
            Rect dividerRect = new Rect(area.width * ratio, area.y, 1f, area.height);
            if (showDivider)
            {
                Color previousColor = GUI.color;
                GUI.color = Color.black;
                GUI.DrawTexture(dividerRect, EditorGUIUtility.whiteTexture);
                GUI.color = previousColor;
            }

            EditorGUIUtility.AddCursorRect(resizeHandleRect, MouseCursor.ResizeHorizontal);

            if (Event.current.type == EventType.MouseDown && resizeHandleRect.Contains(Event.current.mousePosition))
                resize = true;
            if (resize)
            {
                ratio = Event.current.mousePosition.x / area.width;
                GUI.changed = true;
            }

            if (Event.current.type == EventType.MouseUp && resize)
            {
                resize = false;
                EditorPrefs.SetFloat(Key_SplitRatio, ratio);
            }
        }
    }
}

