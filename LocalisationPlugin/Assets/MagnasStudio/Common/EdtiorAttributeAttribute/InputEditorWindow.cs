﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace  MagnasStudio.Editors
{
    public class InputEditorWindow : EditorWindow
    {
        private string mLabel;
        private string mValue;
        private Action<string> mOnSave;

        public void Show(Rect buttonRect, string label, Action<string> callback)
        {
            mLabel = label;
            mOnSave = callback;
            mValue = string.Empty;
            Vector2 screenMousePosition = GUIUtility.GUIToScreenPoint(Event.current.mousePosition);
            buttonRect.center = screenMousePosition;
            ShowAsDropDown(buttonRect, new Vector2(380, 45));
        }

        private void OnGUI()
        {
            mValue = EditorGUILayout.TextField(mLabel, mValue);
            if (GUILayout.Button("Save") && !string.IsNullOrEmpty(mValue))
            {
                mOnSave?.Invoke(mValue);
                Close();
            }
        }
    }
}