﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine.PlayerLoop;

namespace  MagnasStudio.Editors
{
    public abstract class ConfigEditor : EditorWindow
    {
        public Vector2 ScrollPosition;

        protected static InputEditorWindow mInputEditorWindow;

        public InputEditorWindow pInputEditorWindow
        {
            get
            {
                if (mInputEditorWindow == null)
                    mInputEditorWindow = CreateInstance<InputEditorWindow>();
                return mInputEditorWindow;
            }
        }

        protected Color pHighlightGreen => EditorGUIUtility.isProSkin ? new Color32(144, 238, 144, 255) : new Color32(11, 102, 35, 255);
        protected Color pHighlightRed =>  EditorGUIUtility.isProSkin ? new Color32(222, 23, 56, 255) : new Color32(139, 0, 0, 255);

        protected string pFilePath => Path.Combine(Application.temporaryCachePath, name + ".json");

        public string pSearchParameter { get; set; } = string.Empty;

        private GUIStyle mButtonStyle;

        public GUIStyle pButtonStyle
        {
            get
            {
                if (mButtonStyle == null)
                {
                    mButtonStyle = new GUIStyle();
                    mButtonStyle.normal.background = this.GetInPathTexture("Gizmos/DarkOrangeULine.png");
                    mButtonStyle.onNormal.background =
                        mButtonStyle.active.background = this.GetInPathTexture("Gizmos/LightBlueULine.png");
                    mButtonStyle.normal.textColor = EditorStyles.label.normal.textColor;
                    mButtonStyle.onNormal.textColor = Color.blue;
                    mButtonStyle.active.textColor = EditorStyles.label.active.textColor;
                    mButtonStyle.alignment = TextAnchor.MiddleCenter;
                    mButtonStyle.imagePosition = ImagePosition.ImageAbove;
                    mButtonStyle.margin = new RectOffset(3, 3, 10, 10);
                    mButtonStyle.border = new RectOffset(4, 4, 4, 4);
                }
                return mButtonStyle;
            }
        }

        protected void DrawDivider()
        {
            Color previousColor = GUI.color;
            GUI.color = Color.black;
            Rect dividerRect = EditorGUILayout.GetControlRect(GUILayout.Height(2));
            GUI.DrawTexture(dividerRect, EditorGUIUtility.whiteTexture);
            GUI.color = previousColor;
        }
        
        protected void SetObjectDirty(UnityEngine.Object obj, string name)
        {
            if (!string.IsNullOrEmpty(name))
                Undo.RecordObject(obj, name);
            EditorUtility.SetDirty(obj);
        }

        public void LoadPreference()
        {
            if (File.Exists(pFilePath))
            {
                string json = File.ReadAllText(pFilePath);
                JsonUtility.FromJsonOverwrite(json, this);
            }
        }

        public void SavePreference()
        {
            string json = JsonUtility.ToJson(this);
            File.WriteAllText(pFilePath, json);
        }

        protected string Title(Rect rect, string label, ref bool folded, bool header = true, bool useFold = false, params string[] commands)
        {
            string option = string.Empty;
            float commandWidth = 22;

            rect.width -= (commands.Length * commandWidth);
            Rect crect = new Rect(rect);
            crect.width -= 22;

            if (useFold)
                folded = EditorGUI.Foldout(crect, folded, label);
            else
                EditorGUI.LabelField(crect, label, header ? EditorStyles.boldLabel : EditorStyles.label);

            rect.x += rect.width;
            rect.width = commandWidth;
            foreach (string command in commands)
            {
                if (GUI.Button(rect, command))
                    option = command;
                rect.x += commandWidth;
            }
            return option;
        }

        protected string[] GetOptionsWithNone(IEnumerable<string> enumerableOptions)
        {
            List<string> options = enumerableOptions.ToList();
            options.Insert(0, "None");
            return options.ToArray();
        }

        protected string Popup(Rect rect, string label, string value, string[] options)
        {
            int index = Array.IndexOf(options, value);
            index = Mathf.Clamp(index, 0, options.Length - 1);
            index = EditorGUI.Popup(rect, label, index, options);
            string result = options.Length > index ? options[index] : "None";
            return result == "None" ? string.Empty : result;
        }

        public void EndUpdate()
        {
            LastUpdate();
        }

        protected virtual void LastUpdate()
        {
            
        }
    }
}
