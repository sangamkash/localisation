﻿using System;
using UnityEngine;
using UnityEditor;

namespace  MagnasStudio.Editors
{
    public class ConfigEditorAttribute : Attribute
    {
        public string Name;
        public int Priority { get; set; }
        public Type Type { get; set; }

        public ConfigEditorAttribute(string name)
        {
            Name = name;
        }
    }
}