﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace  MagnasStudio.Editors
{
    public static class EditorWindowExtension 
    {
        public static Texture2D GetInPathTexture(this ScriptableObject window, string fileName)
        {
            string path = Path.GetDirectoryName(AssetDatabase.GetAssetPath(MonoScript.FromScriptableObject(window)));
            string director = Path.GetDirectoryName(path);
            string iconPath = Path.Combine(director, fileName);
            Texture2D texure = AssetDatabase.LoadAssetAtPath<Texture2D>(iconPath);
            return texure;
        }
    }
}