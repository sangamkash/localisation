﻿//Created by Magnas studio on 2/05/2020
using UnityEngine;

namespace MagnasStudio
{
    public enum LogType
    {
        High,
        Medium,
        Low
    }
    public class MsDebug
    {
        public static readonly string[] LOGTYPES = { HIGH_COLOR, MEDIUM_COLOR, LOW_COLOR };
        public const string HIGH_COLOR = "#ff0000";//Red
        public const string MEDIUM_COLOR = "#ff00ff";//Purpal
        public const string LOW_COLOR = "#ffff00";//yellow
        public const string TAG_COLOR = "#ff9933";//Orange
        public const string TAG_DEFAULT = "Default";

        public static void Log(object message, string tag = TAG_DEFAULT, LogType logType = LogType.Low)
        {
            Debug.Log(string.Format("<color={0}>Tag:: {1} </color>||<color{2}>Message:: {3}</color>", TAG_COLOR, tag, LOGTYPES[(int)logType], message));
        }

        public static void LogWarning(object message, string tag = TAG_DEFAULT, LogType logType = LogType.Low)
        {
            Debug.LogWarning(string.Format("<color={0}>Tag:: {1} </color>||<color{2}>Message:: {3}</color>", TAG_COLOR, tag, LOGTYPES[(int)logType], message));
        }

        public static void LogError(object message, string tag = TAG_DEFAULT, LogType logType = LogType.Low)
        {
            Debug.LogError(string.Format("<color={0}>Tag:: {1} </color>||<color{2}>Message:: {3}</color>", TAG_COLOR, tag, LOGTYPES[(int)logType], message));
        }
    }
}