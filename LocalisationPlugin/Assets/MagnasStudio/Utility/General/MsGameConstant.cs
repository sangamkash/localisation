﻿using UnityEngine;

namespace MagnasStudio.GameConstants
{
    public class MsGameConstant
    {
        //Vector2
        public static readonly Vector3 VECTOR2_UP = Vector2.up;
        public static readonly Vector3 VECTOR2_DOWN = Vector2.down;
        public static readonly Vector3 VECTOR2_LEFT = Vector2.left;
        public static readonly Vector3 VECTOR2_RIGHT = Vector2.right;
        public static readonly Vector3 VECTOR2_Zero = Vector2.zero;
        public static readonly Vector3 VECTOR2_ONE = Vector2.one;

        //Vector3
        public static readonly Vector3 VECTOR3_UP = Vector3.up;
        public static readonly Vector3 VECTOR3_DOWN = Vector3.down;
        public static readonly Vector3 VECTOR3_LEFT = Vector3.left;
        public static readonly Vector3 VECTOR3_RIGHT = Vector3.right;
        public static readonly Vector3 VECTOR3_Zero = Vector3.zero;
        public static readonly Vector3 VECTOR3_One = Vector3.right;

    }
}
