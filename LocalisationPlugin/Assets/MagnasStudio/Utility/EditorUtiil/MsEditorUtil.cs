﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using static UnityEngine.MonoBehaviour;
using Object = UnityEngine.Object;

#if UNITY_EDITOR
namespace MagnasStudio.Util.Editors
{
    [Serializable]
    public enum ObjectType
    {
        SceneObject,
        Prefab,
        IndividualAsset
    }
    [Serializable]
    public class PrefabData
    {
        public GameObject gameObject;
        public string guid;
        public string path;
    }

    [Serializable]
    public class HierarchyPath
    {
        public int hierarchyIndex;
        public string name;
    }
    
    [Serializable]
    public class UnityObjectData
    {
        public ObjectType objectType;
        public HierarchyPath[] path;
        public string objectPath;
        
        public override string ToString()
        {
            return this.ToString("/");
        }
    }
    public static class MsEditorUtil
    {
        private const string PREFAB_TYPE = "t:Prefab";
        
        //Prefab==================
        public static List<T> FindAssetsByType<T>() where T : Object
        {
            List<T> assets = new List<T>();
            string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
            for (int i = 0; i < guids.Length; i++)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
                T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
                if (asset != null)
                {
                    assets.Add(asset);
                }
            }

            return assets;
        }

        public static string[] FindAllAssetPathByType<T>() where T : Object
        {
            return AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
        }

        public static List<PrefabData> FindAllPrefabs()
        {
            var prefabList= new List<PrefabData>();
            string[] guids = AssetDatabase.FindAssets(PREFAB_TYPE);
            for (int i = 0; i < guids.Length; i++)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
                var asset = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
                if (asset != null)
                {
                    prefabList.Add(new PrefabData()
                    {
                        gameObject = asset,
                        guid = guids[i],
                        path = assetPath
                    });
                }
            }
            return prefabList;
        }
        
        //Prefab===================
        
        public static List<UnityObjectData> FindObjectByType<T>() where T : MonoBehaviour
        {
            var list = new List<UnityObjectData>();
            var objArray = Object.FindObjectsOfType<T>();
            foreach (var item in objArray)
            {
                
            }
            return list;
        }

        public static UnityObjectData GetObjectData(this Object obj, ObjectType objectType,string location)
        {
            switch (objectType)
            {
                case ObjectType.IndividualAsset:
                    return obj.GetIndividualObjectData(location);
                case ObjectType.Prefab:
                    return obj.GetPrefabObjectData(location);
                case ObjectType.SceneObject:
                    return obj.GetSceneObjectData(location);
            }

            return null;
        }

        private static UnityObjectData GetSceneObjectData(this object obj,string location)
        {
            var pathArray = new List<HierarchyPath>();
            var temp = ((GameObject)obj).transform;
            while (temp != null)
            {
                pathArray.Add(new HierarchyPath()
                {
                    name = temp.name,
                    hierarchyIndex = temp.GetSiblingIndex()
                });

                temp = temp.parent;
            }

            pathArray.Reverse();
            var sceneObjectData = new UnityObjectData()
            {
                objectType = ObjectType.SceneObject,
                path = pathArray.ToArray(),
                objectPath = location
            };
            return sceneObjectData;
        }
        
        private static UnityObjectData GetIndividualObjectData(this Object obj,string location)
        {
            var pathArray = new List<HierarchyPath>();
            var temp = ((GameObject)obj).transform;
            while (temp != null)
            {
                pathArray.Add(new HierarchyPath()
                {
                    name = temp.name,
                    hierarchyIndex = temp.GetSiblingIndex()
                });

                temp = temp.parent;
            }

            pathArray.Reverse();
            var sceneObjectData = new UnityObjectData()
            {
                objectType = ObjectType.IndividualAsset,
                path = pathArray.ToArray(),
                objectPath = location
            };
            return sceneObjectData;
        }
        private static UnityObjectData GetPrefabObjectData(this Object obj,string location)
        {
            var pathArray = new List<HierarchyPath>();
            var temp = ((GameObject)obj).transform;
            while (temp != null)
            {
                pathArray.Add(new HierarchyPath()
                {
                    name = temp.name,
                    hierarchyIndex = temp.GetSiblingIndex()
                });

                temp = temp.parent;
            }

            pathArray.Reverse();
            var sceneObjectData = new UnityObjectData()
            {
                objectType = ObjectType.Prefab,
                path = pathArray.ToArray(),
                objectPath = location
            };
            return sceneObjectData;
        }
        
        //Extension 
        public static string ToString(this UnityObjectData data, string separator)
        {
            var str = "";
            for (var i = 0; i < data.path.Length; i++)
            {
                if (i == data.path.Length - 1)
                    str += data.path[i].name;
                else
                    str += data.path[i].name + separator;
            }
            return str;
        }

   
        
        private static GameObject GetSceneGameObject(this UnityObjectData unityObjectData)
        {
            var activeScene = SceneManager.GetActiveScene();
            var rootObjs = activeScene.GetRootGameObjects();
            var totalRootCount = rootObjs.Length;
            var count = unityObjectData.path.Length;
            Transform transform = null;
            if (count > 0 && totalRootCount > 0)
            {
                for (var i = 0; i < count; i++)
                {
                    var index= unityObjectData.path[i].hierarchyIndex;
                    if (i == 0 && index < totalRootCount)
                    {
                       transform = rootObjs[index].transform;
                    }
                    else if(i >0 && index< transform.childCount)
                    {
                        transform = transform.GetChild(index);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return transform.gameObject;
        }
        
        public static GameObject GetPrefabGameObject(this UnityObjectData unityObjectData)
        {
            var activeScene = SceneManager.GetActiveScene();
            var rootObjs = activeScene.GetRootGameObjects();
            if (rootObjs.Length <= 0)
                return null;
            var rootObj = rootObjs[0];
            var count = unityObjectData.path.Length;
            Transform transform = null;
            if (count > 0)
            {
                for (var i = 0; i < count; i++)
                {
                    var index= unityObjectData.path[i].hierarchyIndex;
                    if (i == 0)
                    {
                        transform = rootObj.transform;
                    }
                    else if(i >0 && index< transform.childCount)
                    {
                        transform = transform.GetChild(index);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return transform.gameObject;
        }
        
        
        public static void HighLight(this UnityObjectData unityObjectData)
        {
            if (!string.IsNullOrEmpty(unityObjectData.objectPath))
            {
                switch (unityObjectData.objectType)
                {
                    case ObjectType.IndividualAsset:
                        var obj=AssetDatabase.LoadAssetAtPath<Object>(unityObjectData.objectPath);
                        EditorGUIUtility.PingObject(obj);
                        break;
                    case ObjectType.Prefab:
                        var prefab=AssetDatabase.LoadAssetAtPath<GameObject>(unityObjectData.objectPath);
                        var previewScene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene);
                        PrefabUtility.InstantiatePrefab(prefab, previewScene);
                        EditorGUIUtility.PingObject(unityObjectData.GetPrefabGameObject());
                        break;
                    case ObjectType.SceneObject:
                        EditorSceneManager.OpenScene(unityObjectData.objectPath);
                        EditorGUIUtility.PingObject(unityObjectData.GetSceneGameObject());
                        break;
                }
               
            }
            
        }
    }
}

#endif
