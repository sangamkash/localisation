﻿using UnityEditor;
using UnityEngine;

namespace MagnasStudio.Editors.Introduction
{
    [ConfigEditor("Introduction", Priority = 10000)]
    public class IntroductionEditor : ConfigEditor
    {
        private GUIStyle headerFolded;
        private void OnEnable()
        {
            InitializeGUIStyles();
        }

        private void InitilizeGUI()
        {
           
        }

        #region Draw
        private void OnGUI()
        {
            
        }

        protected void OnFooterGUI()
        {
            GUILayout.Space(5);
            EditorGUILayout.BeginHorizontal();

            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Refresh", GUILayout.Width(80), GUILayout.Height(30)))
            {
               
            }
            GUILayout.Space(10);
            if (GUILayout.Button("Push", GUILayout.Width(80), GUILayout.Height(30)))
            {
                
            }
            GUILayout.Space(10);
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(3);
        }

        #endregion

        private void InitializeGUIStyles()
        {
            headerFolded = new GUIStyle(EditorStyles.foldout);
            headerFolded.fontStyle = FontStyle.Bold;
        }
      
    }

}
